#此工具用于把excel中程序的配置信息转化成为yaml格式配置文件，供wine助手调用
#python后面跟的第一个参数是需要转化excel文件，使用方法为：
# xlsx2yamlplus.py software_info_example.xlsx
import openpyxl
import yaml
import sys
import os
import shutil
from tabulate import basestring
#对应win-program中对应用的分类，生成相应文件夹保存对应种类的yml文件
folder_path = ["Game","Software"]
for i in range(len(folder_path)):
    if os.path.exists(folder_path[i]):
        continue;
    else:
        os.makedirs(folder_path[i])

#将生成的.yml文件移入对应文件夹
def moveFiles(file_name,category):
    source_file = os.path.join("./", file_name)
    destination_folder = "./" + category
    destination_file = os.path.join(destination_folder, os.path.basename(source_file))
    if os.path.exists(destination_file):
        os.remove(destination_file)
    shutil.move(source_file, destination_folder)
#用于定义需要的自定义 YAML 输出格式
class IndentedListDumper(yaml.Dumper):
    #重写控制缩进方法
    def increase_indent(self, flow=False, indentless=False):
        return super().increase_indent(flow, False)
#生成带有缩进列表的 YAML 内容
def dump_with_indented_list(data):
    def represent_list(self, data):
        return self.represent_sequence('tag:yaml.org,2002:seq', data, flow_style=False)

    IndentedListDumper.add_representer(list, represent_list)

    return yaml.dump(data, Dumper=IndentedListDumper, sort_keys=False, indent=2, allow_unicode=True)
# 打开 Excel 文件
args = sys.argv[1:]
if args:
    print('正在转换 《 {} 》文件'.format(args[0]))
    workbook = openpyxl.load_workbook(args[0])
else :
    print('找不到要转换的execel文件，请使用以下格式执行脚本：xlsx2yamlplus.py software_info_example.xlsx')
    sys.exit(1)
    #workbook = openpyxl.load_workbook('software_info_example.xlsx')
sheet = workbook['software_upload_list']
# 获取 Excel 文件第一行元素作为 YAML 文件的 key
keys = [cell.value for cell in sheet[1]]
for i, key in enumerate(keys, start=0):
    print(i ,key)


#定义全局变量total，用于保存xls2yml的所需数据
total={}
filename1 = f"xls2index.yml"
# 遍历 Excel 文件的每一行，生成对应的 YAML 文件
for row in sheet.iter_rows(min_row=2): #从第二行开始遍历
    # 获取该行第一个元素作为 YAML 文件名
    filename = f"{row[0].value}.yml"
    # filename = f"xls2index.yml"
    # 构建 YAML 文件内容

    #indexdata作为缓冲集合，为total提供数据
    indexdata = {}

    # data = OrderedDict()
    data = {}
    data['Name'] = '...'
    data['Description'] = '...'
    data['Grade'] = '...'
    data['Arch'] = 'win32'
    data['LinuxArch'] = ['x86_64','aarch64']
    data['Dependencies'] = ['..','..']
    data['Executable'] = {'name':  '..' , 'path': "..",   'wineenv': "..",  'arguments': ".."}  
    data['Steps'] =[{'action':  'install', 'file_type':  '..',   'file_name': "..",  'url': "..",  'file_checksum': "..",  'arguments': ".."} ,{'action': "run_script", 'filename': "..",   'script': ".."}]
    #data = {}

    #
    indexdata['Arch'] = ''
    indexdata['Name'] = ''
    indexdata['Description'] = ''
    indexdata['Grade'] = ''

    indexdata['Category'] = ''
    indexdata['Icon'] = None
    indexdata['LinuxArch'] = ''
    indexdata['Publish'] = ''
    continue_flag = False
    for i, cell in enumerate(row, start=0):
        if keys[i] == 'action_install_url' :
            data['Steps'][0]['url'] = cell.value
        elif keys[i] == 'Name' :
            if cell.value :
                data['Name'] = indexdata['Name'] = cell.value
            else :
                continue_flag = True
                continue
            #indexdata['Icon'] = indexdata['Name'] + '.png'
        elif keys[i] == 'Arch':
            indexdata['Arch']  = data['Arch'] = cell.value
        elif keys[i] == 'Description':
            data['Description'] = indexdata['Description'] = cell.value
        elif keys[i] == 'Grade':
            data['Grade'] = indexdata['Grade'] = cell.value
        elif keys[i] == 'LinuxArch' :
            indexdata['LinuxArch'] = data['LinuxArch'] = str(cell.value).split(",")
        elif keys[i] == 'Dependencies' :
            data['Dependencies'] = str(cell.value).split(",")
        elif keys[i] == 'Executable_name' :
            data['Executable']['name'] = cell.value
        elif keys[i] == 'Executable_path' :
            if cell.value == 'null':
                data['Executable']['path'] = None
            else :
                data['Executable']['path'] = cell.value
        elif keys[i] == 'Executable_wineenv' :
            if cell.value == 'null':
                data['Executable']['wineenv'] = None
            else :
                data['Executable']['wineenv'] = cell.value
        elif keys[i] == 'Executable_arguments' :
            if cell.value == 'null':
                data['Executable']['arguments'] = None
            else :
                data['Executable']['arguments'] = cell.value
        elif keys[i] == 'action1' :
            data['Steps'][0]['action'] = cell.value
        elif keys[i] == 'action_install_file_type' :
            data['Steps'][0]['file_type'] = cell.value
        elif keys[i] == 'action_install_file_path' :
            data['Steps'][0]['file_name'] = cell.value
        elif keys[i] == 'url' :
            data['Steps'][0]['url'] = cell.value
        elif keys[i] == 'action_install_file_checksum' :
            data['Steps'][0]['file_checksum'] = cell.value
        elif keys[i] == 'action_install_arguments' :
            if cell.value == 'null':
                data['Steps'][0]['arguments'] = None
            else :
                data['Steps'][0]['arguments'] = cell.value
        elif keys[i] == 'install_protable' :
            if isinstance(cell.value, bool):
                data['Steps'][0]['protable'] = cell.value
            elif isinstance(cell.value, basestring):
                if cell.value.capitalize() == "True":
                    data['Steps'][0]['protable'] = True
                elif cell.value.capitalize() == "False":
                    data['Steps'][0]['protable'] = False
        elif keys[i] == 'action2' :
            data['Steps'][1]['action'] = cell.value
        elif keys[i] == 'action_run_script_filename' :
            data['Steps'][1]['filename'] = cell.value
        elif keys[i] == 'action_run_script_script' :
            data['Steps'][1]['script'] = cell.value
        elif keys[i] == 'Publish' :
            if isinstance(cell.value, bool):
                data[keys[i]] = indexdata['Publish'] = cell.value
            elif isinstance(cell.value, basestring):
                if cell.value.capitalize() == "True":
                    data[keys[i]] = indexdata['Publish'] = True
                elif cell.value.capitalize() == "False":
                    data[keys[i]] = indexdata['Publish'] = False
        elif keys[i] == 'Category':
            indexdata['Category'] = cell.value
#        else :
            #data[keys[i]] = cell.value
    if continue_flag == True :
        continue
      
    total[indexdata['Name']] = indexdata

    # 将 YAML 数据转化为字符串并写入文件
    # print(total)
    # with open(filename, 'wb') as f:
    #     # print('this is yaml dump')
    #     print(yaml.dump(data, sort_keys=False))
    #     f.write(yaml.dump(data, sort_keys=False, encoding='utf-8', allow_unicode=True, indent=2))

    with open(filename, 'w',encoding='utf-8') as f:
        # print('this is yaml dump')
        print(yaml.dump(data, sort_keys=False))
        f.write(dump_with_indented_list(data))
    #根据类别，将生成的 xxx.yml移入相应的文件夹
    if indexdata['Category'] == 'Game':
        moveFiles(filename, indexdata['Category'])
    elif indexdata['Category'] == 'Software':
        moveFiles(filename,indexdata['Category'])
    else:
        print("应用类别有误")

with open(filename1, 'w', encoding='utf-8') as f:
    print(dump_with_indented_list(total))
    f.write(dump_with_indented_list(total))


