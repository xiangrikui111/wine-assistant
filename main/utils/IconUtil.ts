/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import { spawn } from "child_process";
import { logger } from "./Logger";
import * as path from "path";
import * as fs from "fs";
import * as mime from "mime";
import { WorkDirConfig } from "../commons/config";
import { rimraf } from "rimraf";
import stringRandom from "string-random";

export class ExportIcon {
  private static _defaultIcon = ""; //预留一个默认的icon变量，待后续有icon时候可以加上
  private static _wrestool = "/usr/bin/wrestool";
  private static _identify = "/usr/bin/identify";
  private static _convert = "/usr/bin/convert";
  private static _icotool = "/usr/bin/icotool";
  private static _mismatchReg =
    /wrestool: (.*): mismatch of size in icon resource (.{1})-(\d+)(.{1}) and group \((\d+) vs (\d+)\)/i;

  private static convert(cwd: string, idName: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      //convert "Bomberic2.exe_3_3" -thumbnail 256x256 -alpha on -background none -flatten "/path/to/output.png"

      let prefix = stringRandom(4);
      let iconName = prefix + "_" + idName + ".png";
      let outputFile = path.join(WorkDirConfig.IconsDir, iconName);
      if (fs.existsSync(outputFile)) {
        fs.unlinkSync(outputFile);
      }
      let proc = spawn(
        this._convert,
        [
          idName,
          /*
          "-thumbnail",
          "256x256",
          "-alpha",
          "on",
          "-background",
          "none",
          "-flatten",
          */
          outputFile,
        ],
        { cwd: cwd }
      );
      proc.stderr.on("data", (err) => {
        reject(err);
      });
      proc.on("error", (err) => {
        reject(err.message);
      });
      proc.on("exit", (code) => {
        if (code == 0) {
          resolve(outputFile);
        } else {
          reject(`Exec ${this._convert} fail`);
        }
      });
      return;
    });
  }

  private static icoIdentify(
    icoPath: string,
    mismatchIds: number[]
  ): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      //identify icoPath
      let cwd = path.dirname(icoPath);
      let icoName = path.basename(icoPath);
      const proc = spawn(this._identify, [icoName], { cwd: cwd });
      proc.stderr.on("data", (err) => {
        logger.error(err);
      });
      //let
      //let ids = [] as string[];
      let maxAssets = [] as (string | number)[][];
      proc.stdout.on("data", (out) => {
        out
          .toString()
          .split("\n")
          .forEach((line, lineIdx) => {
            line = line.replaceAll("\r", "");
            if (line == "") {
              return;
            }
            const reg = /(.*) (\w*) (\d*)x(\d*) (.*)/;
            if (!reg.test(line)) {
              return;
            }
            let matches = line.match(reg);
            let name = matches[1];
            let type = matches[2];
            let idx = "0"; //matches[2];
            let idxMatche = name.match(/(.*)\[(\d*)\]/);
            if (idxMatche) {
              idx = idxMatche[2];
            }
            let currLineX = matches[4];

            if (mismatchIds.indexOf(lineIdx + 1) >= 0) {
              return;
            }
            maxAssets.push([
              name,
              Number.parseInt(idx),
              type,
              Number.parseInt(currLineX),
            ]);
          });
      });
      const convertToPng = () => {
        let maxAsset = maxAssets.pop();
        if (maxAsset == undefined) {
          logger.debug("Extract png fail : ", icoPath);
          reject("Extract png fail");
          return;
        }
        let type = maxAsset[2];
        let idx = maxAsset[1] as number;
        let name = maxAsset[0];

        if (type == "PNG") {
          this.icotoolExtract(icoPath, idx + 1).then(
            (res) => {
              resolve(res);
            },
            (reason) => {
              logger.error("Try icotool extract fail : ", reason, maxAsset);
              convertToPng();
            }
          );
        } else {
          this.convert(cwd, `${name}`)
            .then((result) => {
              resolve(result);
            })
            .catch((reason) => {
              logger.error("Try convert extract fail : ", reason, maxAsset);
              convertToPng();
            });
        }
      };
      proc.on("exit", (code) => {
        if (code == 0) {
          if (maxAssets.length > 0) {
            maxAssets = maxAssets.sort((a, b) => {
              let ax = a[3] as number;
              let bx = b[3] as number;
              return ax - bx;
            });
            logger.debug(maxAssets);
            convertToPng();
            /*
            if (maxAsset[2] == "PNG") {
              this.icotoolExtract(icoPath, (maxAsset[1] as number) + 1).then(
                (res) => {
                  resolve(res);
                },
                (reason) => {
                  logger.debug(reason);
                  this.convert(cwd, `${maxIcoAsset[0]}[${maxIcoAsset[1]}]`)
                    .then((result) => {
                      resolve(result);
                    })
                    .catch((reason) => {
                      reject(reason);
                    });
                }
              );
            } else {
              this.convert(cwd, `${maxIcoAsset[0]}[${maxIcoAsset[1]}]`)
                .then((result) => {
                  resolve(result);
                })
                .catch((reason) => {
                  this.icotoolExtract(
                    icoPath,
                    (maxAsset[1] as number) + 1
                  ).then(
                    (res) => {
                      resolve(res);
                    },
                    (reason) => {
                      reject(reason);
                    }
                  );
                });
            }
            */
          } else {
            resolve(this._defaultIcon);
          }
        } else {
          reject(`Exec ${this._identify} fail`);
        }
      });
    });
  }

  private static icotoolExtract(icoPath: string, idx: number) {
    let cwd: string = path.dirname(icoPath);
    let icoName = path.basename(icoPath);
    let prefix = stringRandom(4);
    let outputFile = path.join(
      WorkDirConfig.IconsDir,
      `${prefix}_${icoName}.png`
    );
    if (fs.existsSync(outputFile)) {
      fs.unlinkSync(outputFile);
    }
    return new Promise<string>((resolve, reject) => {
      const proc = spawn(
        this._icotool,
        ["-x", "-i", idx + "", "-o", outputFile, icoName],
        {
          cwd: cwd,
        }
      );
      proc.stderr.on("data", (err) => {
        logger.debug(err.toString());
      });
      proc.on("error", (err) => {
        logger.error(err.message);
      });
      proc.on("exit", (code) => {
        if (code == 0) {
          resolve(outputFile);
        } else {
          reject("Exec icotool -x fail " + code);
        }
      });
    });
  }
  private static extractGroupIcon(
    t14s: string[][],
    programPath: string
  ): Promise<string> {
    let cwd = path.dirname(programPath);
    let pName = path.basename(programPath);
    let tempIcons = "./.tempicons/";
    let tempExtractPath = path.join(cwd, tempIcons);
    if (fs.existsSync(tempExtractPath)) {
      rimraf.rimrafSync(tempExtractPath);
    }
    fs.mkdirSync(tempExtractPath, { recursive: true });
    return new Promise<string>((resolve, reject) => {
      const proc = spawn(
        this._wrestool,
        ["-x", "-t14", `--output=${tempIcons}`, pName],
        { cwd: cwd }
      );
      let mismatchIds = [] as number[];
      proc.stderr.on("data", (err) => {
        logger.error(err.toString());
        let str = err.toString();
        if (this._mismatchReg.test(str)) {
          mismatchIds.push(Number.parseInt(str.match(this._mismatchReg)[3]));
        }
      });
      proc.on("error", (err) => {
        reject(err.message);
      });
      proc.on("exit", (code) => {
        if (code == 0) {
          let icoDir = path.join(cwd, tempIcons);
          let icoPath = `${icoDir}${pName}_${t14s[0][0]}_${t14s[0][1]}_${t14s[0][2]}.ico`; //programPath + ".ico";
          if (!fs.existsSync(icoPath)) {
            icoPath = `${icoDir}${pName}_${t14s[0][0]}_${t14s[0][1]}.ico`; //programPath + ".ico";
            if (!fs.existsSync(icoPath)) {
              reject(`Exec ${this._wrestool} -x fail: ${icoPath}`);
              return;
            }
          }

          /*
          let prefix = stringRandom(4);
          let dist = path.join(
            WorkDirConfig.IconsDir,
            `${prefix}_${pName}.ico`
          );
          if (fs.existsSync(dist)) {
            fs.unlinkSync(dist);
          }
          fs.renameSync(icoPath, dist);
          resolve(dist);
          */
          let mimeType = mime.getType(icoPath);
          if (mimeType === "image/vnd.microsoft.icon") {
            this.icoIdentify(icoPath, mismatchIds)
              .then((result) => {
                resolve(result);
              })
              .catch((reason) => {
                reject(reason);
                /*
                this.icotoolExtract(icoPath).then(
                  (result) => {
                    resolve(result);
                  },
                  (reason) => {
                    reject(reason);
                  }
                );
                */
              });
          } else {
            let dist = path.join(WorkDirConfig.IconsDir, pName + ".png");
            if (fs.existsSync(dist)) {
              fs.unlinkSync(dist);
            }
            fs.renameSync(icoPath, dist);
            resolve(dist);
          }
        } else {
          reject(`Exec ${this._wrestool} -x fail`);
        }
      });
    });
  }
  static getIcon(programPath: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      if (!fs.existsSync(programPath)) {
        reject("Program not found");
        return;
      }
      let cwd = path.dirname(programPath);

      let pName = path.basename(programPath);
      const proc = spawn(this._wrestool, ["-l", "-t14", pName], {
        cwd: cwd,
      });
      proc.stderr.on("data", (data) => {
        logger.error(data.toString());
      });
      let t14line = [] as string[][];
      proc.stdout.on("data", (data) => {
        data
          .toString()
          .split("\n")
          .forEach((line) => {
            const reg = /--type=(.*) --name=(.*) --language=(.*) \[(.*)\]/;

            let type = "";
            let name = "";
            let lang = "";
            if (reg.test(line)) {
              const matches = line.match(reg);
              type = matches[1].replaceAll("'", "");
              name = matches[2].replaceAll("'", "");
              lang = matches[3].replaceAll("'", "");
              t14line.push([type, name, lang]);
            }

            logger.debug(t14line);
          });
      });
      proc.on("error", (err) => {
        reject(err.message);
      });
      proc.on("exit", (code) => {
        if (code == 0) {
          if (t14line.length > 0) {
            //let outputDir = "/tmp/"
            this.extractGroupIcon(t14line, programPath)
              .then((result) => {
                resolve(result);
              })
              .catch((reason) => {
                reject(reason);
              });
          } else {
            resolve(this._defaultIcon);
          }
        } else {
          reject(`Exec ${this._wrestool} -l fail`);
        }
      });
    });
  }
}
