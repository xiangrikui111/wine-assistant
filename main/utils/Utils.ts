/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import crypto from "crypto";
import { WorkDirConfig } from "../commons/config";
import * as fs from "fs";
import * as path from "path";
import { logger } from "./Logger";
import { ExportIcon } from "./IconUtil";

export class Crypto {
  static md5(data: any): string {
    let hash = crypto.createHash("md5");
    return hash.update(data).digest("hex");
  }
}

export function generateDesktop(
  name: string,
  comment: string,
  cmd: string,
  programPath: string
): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    let desktopName = `${name}.desktop`;

    const saveFile = (iconPath: string) => {
      const desktopFileContent = `[Desktop Entry]
Type=Application
StartupNotify=true
Name=${name}
Comment=${comment}
Exec=${cmd}
Icon=${iconPath}
Terminal=false
`;
      let appsPath = path.join(WorkDirConfig.ApplicationsDir, desktopName);
      let desktopPath = path.join(WorkDirConfig.DesktopDir, desktopName);

      try {
        if (fs.existsSync(desktopPath)) {
          fs.unlinkSync(desktopPath);
        }
        if (fs.existsSync(appsPath)) {
          fs.unlinkSync(appsPath);
        }
        fs.writeFileSync(desktopPath, desktopFileContent, { mode: 0o755 });
        fs.writeFileSync(appsPath, desktopFileContent, { mode: 0o755 });
        logger.debug("on create", cmd, iconPath);
      } catch (error) {
        logger.error("on create", error);
      }
    };
    ExportIcon.getIcon(programPath).then(
      (iconPath) => {
        saveFile(iconPath);
        resolve();
      },
      (reason) => {
        logger.error("Get icon fail:" + reason);
        saveFile("");
        resolve();
      }
    );
  });
}

export function getIconPath(filePath: string): string {
  try {
    if (fs.existsSync(filePath)) {
      var lines = fs.readFileSync(filePath, "utf8").split("\n");
      for (var i in lines) {
        if (
          lines[i].indexOf("Icon") !== -1 &&
          lines[i].length > "Icon= ".length
        ) {
          return lines[i].split("=")[1].trim();
        }
      }
    }
  } catch (error) {
    logger.error(error);
  }
  return "";
}

export function removeDesktop(name: string) {
  let desktopName = `${name}.desktop`;

  let appsPath = path.join(WorkDirConfig.ApplicationsDir, desktopName);
  let desktopPath = path.join(WorkDirConfig.DesktopDir, desktopName);
  try {
    var iconPath = "";
    if (fs.existsSync(desktopPath)) {
      iconPath = getIconPath(desktopPath);
      fs.unlinkSync(desktopPath);
    }
    if (fs.existsSync(appsPath)) {
      if (iconPath == "") iconPath = getIconPath(appsPath);
      fs.unlinkSync(appsPath);
    }

    if (fs.existsSync(iconPath)) {
      fs.unlinkSync(iconPath);
    }
  } catch (error) {
    logger.error(error);
  }
}

export function loadConfig(filePath: string): string {
  let data = "";
  try {
    if (fs.existsSync(filePath)) {
      data = fs.readFileSync(filePath, "utf8");
    }
  } catch (error) {}
  return data;
}
export function saveConfig(filePath: string, data: string) {
  let fileDir = path.dirname(filePath);
  try {
    //logger.log(filePath, data);
    if (!fs.existsSync(fileDir)) {
      fs.mkdirSync(fileDir, { recursive: true });
    }
    fs.writeFileSync(filePath, data);
  } catch (error) {
    //logger.log(error);
  }
}

export function renameFile(oldPath: string, newPath: string) {
  try {
    const newDir = path.dirname(newPath);
    if (!fs.existsSync(newDir)) {
      fs.mkdirSync(newDir, { recursive: true });
    }
    fs.renameSync(oldPath, newPath);
    logger.log(`File ${oldPath} renamed to ${newPath}`);
  } catch (err: any) {
    logger.error(err.message);
  }
}

export function isDirectory(path: string): boolean {
  try {
    const stats = fs.statSync(path);
    return stats.isDirectory();
  } catch (err: any) {
    logger.error(err.message);
    return false;
  }
}

export function fileExists(path: string): boolean {
  try {
    fs.accessSync(path, fs.constants.F_OK);
    return true;
  } catch (err) {
    return false;
  }
}
/**
 * 将 Linux 风格的路径转义，防止在命令行执行时出现错误。
 * @param path 要转义的 Linux 风格的路径。
 * @returns 转义后的安全字符串。
 */
export function escapeLinuxPath(path: string): string {
  const specialChars = [
    " ",
    "\t",
    "\n",
    "\r",
    "\f",
    "\v",
    ";",
    "&",
    "|",
    "<",
    ">",
    "(",
    ")",
    "$",
    "`",
    "\\",
    '"',
    "'",
  ];

  let escapedPath = "";
  for (let i = 0; i < path.length; i++) {
    const char = path[i];
    if (specialChars.includes(char)) {
      escapedPath += `\\${char}`;
    } else {
      escapedPath += char;
    }
  }

  return escapedPath;
}

/**
 * 删除一个链接及其真实文件。
 * @param filePath 文件或链接的路径。
 */
export function deleteFileOrLink(filePath: string): void {
  try {
    const stats = fs.lstatSync(filePath);

    if (stats.isSymbolicLink()) {
      // 如果是链接，则删除链接及其真实文件
      const realPath = fs.realpathSync(filePath);
      fs.unlinkSync(filePath);
      fs.unlinkSync(realPath);
    } else if (stats.isFile()) {
      // 如果是文件，则直接删除
      fs.unlinkSync(filePath);
    }
  } catch (error: any) {
    if (error.code !== "ENOENT") {
      // 如果文件不存在，则直接返回
      logger.error(`${error.message}`);
    }
  }
}
