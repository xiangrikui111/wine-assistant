/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import { exec } from "child_process";
import { logger } from "./Logger";
import * as fs from "fs";
import { rimraf } from "rimraf";
import * as mkdirp from "mkdirp";

export abstract class ArchiveCmd {
  abstract readonly Cmd: string;
  abstract readonly Args: string;

  /*
   */
  unpack(filePath: string, unpackPath: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      if (fs.existsSync(unpackPath)) {
        rimraf.sync(unpackPath);
      }
      mkdirp.sync(unpackPath);
      exec(
        `${this.Cmd} ${this.Args.replaceAll("#FILEPATH#", filePath).replaceAll(
          "#UNPACKPATH#",
          unpackPath
        )}`,
        (err) => {
          if (err) {
            logger.error("Error:", err);
            reject(err.message);
          } else {
            logger.debug("Extracted:", filePath);
            resolve();
          }
        }
      );
    });
  }
}

export class Zip extends ArchiveCmd {
  readonly Cmd: string = "/usr/bin/unzip";
  readonly Args: string = "-O GBK -o -q '#FILEPATH#' -d '#UNPACKPATH#'";
}
export class Rar extends ArchiveCmd {
  readonly Cmd: string = "/usr/bin/unrar";
  readonly Args: string = "-e -y '#FILEPATH#' '#UNPACKPATH#'";
}
export class _7z extends ArchiveCmd {
  readonly Cmd: string = "/usr/bin/7z";
  readonly Args: string = "x -y '#FILEPATH#' -o'#UNPACKPATH#'";
}
export class Cabextract extends ArchiveCmd {
  readonly Cmd: string = "/usr/bin/cabextract";
  readonly Args: string = "-d '#UNPACKPATH#' -q '#FILEPATH#'";
}
export class Tar extends ArchiveCmd {
  readonly Cmd: string = "/usr/bin/tar";
  readonly Args: string = "-xf '#FILEPATH#' -C '#UNPACKPATH#'";
}
export class Bzip2 extends ArchiveCmd {
  readonly Cmd: string = "/usr/bin/bzip2";
  readonly Args: string = "-d #FILEPATH# -c | tar -xf - -C #UNPACKPATH#";
}

export function UnpackFile(
  archiveType: string,
  filePath: string,
  unpackPath: string
): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    let cmd: ArchiveCmd;

    switch (archiveType) {
      case "zip":
        cmd = new Zip();
        break;
      case "rar":
        cmd = new Rar();
        break;
      case "7z":
        cmd = new _7z();
        break;
      case "cab":
        cmd = new Cabextract();
        break;
      case "gz" || "xz":
        cmd = new Tar();
        break;
      case "bz2":
        cmd = new Bzip2();
        break;
      default:
        reject("Unsupported file type " + archiveType);
        return;
    }
    cmd.unpack(filePath, unpackPath).then(
      () => {
        resolve();
      },
      (reason) => {
        reject(reason);
      }
    );
  });
}
