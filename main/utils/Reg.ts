/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import * as os from "os";
import * as fs from "fs";
import * as Path from "path";
import { WineCommand } from "./WineCommand";
import { ProcessManager } from "./ProcessManager";

export class RegItem {
  key: string;
  value: string;
  value_type: string;
  data: string;

  constructor(key: string, value: string, value_type: string, data: string) {
    this.key = key;
    this.value = value;
    this.value_type = value_type;
    this.data = data;
  }
}

export class Reg {
  private command: string = "reg.exe";
  // private _config: RegItem[] = [];
  //private static instance: Reg;
  private rootKey: string[] = [
    "HKEY_LOCAL_MACHINE",
    "HKEY_CURRENT_USER",
    "HKEY_CLASSES_ROOT",
    "HKEY_USERS",
    "HKEY_CURRENT_CONFIG",
  ];

  // public constructor(ContainerDir?: string) {
  // 	let data = loadConfig(Path.join(WorkDirConfig.ContainerDir, ContainerDir?ContainerDir:"default"));
  //   if (data != "") {
  //     this._config = JSON.parse(data);
  //   }
  //   if (import.meta.env.DEV) this._loaded = true;
  // }

  /*
  public static getInstance(): Reg {
    if (!this.instance) {
      this.instance = new Reg();
    }
    return this.instance;
  }
  */

  public batchAdd(regs: RegItem[]): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      const file_content: string[] = [];
      file_content.push("Windows Registry Editor Version 5.00\n");
      const groups = new Map<string, RegItem[]>();
      for (const item of regs) {
        const group = groups.get(item.key) || [];
        group.push(item);
        groups.set(item.key, group);
      }
      groups.forEach((group, key) => {
        file_content.push(`\n[${key}]`);
        for (const item of group) {
          if (item.value_type !== "") {
            file_content.push(
              `"${item.value}"="${item.value_type}:${item.data}"`
            );
          } else {
            file_content.push(`"${item.value}"="${item.data}"`);
          }
        }
      });
      file_content.join("\n");

      const tmp_reg_filepath = Path.join(
        os.tmpdir(),
        `batch_${Date.now()}.reg`
      );
      fs.writeFileSync(tmp_reg_filepath, file_content.join("\n"));

      //logger.log(`batchAdd ${file_content} -> ${tmp_reg_filepath}`);

      const wineCmd = new WineCommand(
        this.command,
        { WINEDLLOVERRIDES: "mscoree=  mshtml=" },
        `import ${tmp_reg_filepath}`,
        undefined
      );
      await ProcessManager.getInstance().wait_for_process("reg.exe", 5);
      wineCmd.run().then(resolve, reject);
    });
  }

  public add(
    key: string,
    value: string,
    data: string,
    value_type: string | null = null
  ): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      // const config = this._config;
      /*
      logger.log(
        `Adding Key: [${key}] with Value: [${value}] and Data: [${data}] in registry`
      );
      */

      let args = `add '${key}' /v '${value}' /d '${data}' /f`;
      if (value_type !== null) {
        args = `add '${key}' /v '${value}' /t ${value_type} /d '${data}' /f`;
      }

      const wineCmd = new WineCommand(
        this.command,
        { WINEDLLOVERRIDES: "mscoree=  mshtml=" },
        args,
        undefined
      );
      await ProcessManager.getInstance().wait_for_process("reg.exe", 5);
      wineCmd.run().then(resolve, reject);
    });
  }

  public remove(key: string, value: string): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      // const config = this._config;
      // logger.log(`Removing Value: [${value}] from Key: [${key}] in registry`);
      let args = `delete '${key}' /v '${value}' /f`;

      const wineCmd = new WineCommand(
        this.command,
        { WINEDLLOVERRIDES: "mscoree=  mshtml=" },
        args,
        undefined
      );
      await ProcessManager.getInstance().wait_for_process("reg.exe", 5);
      wineCmd.run().then(resolve, reject);
    });
  }

  public importBundle(bundle: { [key: string]: any }): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      const reg_file = Path.join(os.tmpdir(), `batch_${Date.now()}.reg`);
      const lines: string[] = ["REGEDIT4", ""];
      for (const key in bundle) {
        if (bundle.hasOwnProperty(key)) {
          lines.push(`[${key}]`);
          for (const value of bundle[key]) {
            if (value.data === "-") {
              lines.push(`"${value.value}"=-`);
            } else if (value.key_type) {
              lines.push(`"${value.value}"=${value.key_type}:${value.data}`);
            } else {
              lines.push(`"${value.value}"="${value.data}"`);
            }
          }
          lines.push("");
        }
      }
      fs.writeFileSync(reg_file, lines.join("\n"));
      //      logger.debug(`batchAdd ${lines} -> ${reg_file}`);

      const wineCmd = new WineCommand(
        this.command,
        { WINEDLLOVERRIDES: "mscoree=  mshtml=" },
        `import ${reg_file}`,
        undefined
      );
      await ProcessManager.getInstance().wait_for_process("reg.exe", 5);
      wineCmd.run().then(resolve, reject);
    });
  }

  public readKey(Key: string): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      const wineCmd = new WineCommand(
        this.command,
        { WINEDLLOVERRIDES: "mscoree=  mshtml=", WINEDEBUG: "-all" },
        `QUERY '${Key}' /s`
      );
      await ProcessManager.getInstance().wait_for_process("reg.exe", 5);
      wineCmd.run(true).then((output) => {
        resolve(output);
        // console.log(output);
      }, reject);
    });
  }
  public query(key: string, v?: string): Promise<{}> {
    return new Promise<{}>((resolve, reject) => {
      let args = ["QUERY", `"${key}"`];
      if (v) {
        args.push("/v");
        args.push(v);
      } else {
        args.push("/s");
      }
      const wineCmd = new WineCommand(
        this.command,
        { WINEDLLOVERRIDES: "mscoree=  mshtml=", WINEDEBUG: "-all" },
        args.join(" ")
      );
      let result = {};
      wineCmd.run(true).then(
        (output) => {
          let lines = output.split("\r\n");
          let lastKey = "";
          lines.forEach((line) => {
            let start: string = line.split("\\")[0];
            if (this.rootKey.includes(start)) {
              result[line] = {};
              lastKey = line;
            } else if (line.startsWith("    ")) {
              result[lastKey] = line.trimStart().split("    ");
            }
          });
          resolve(result);
        },
        (reason) => {
          reject(reason);
        }
      );
    });
  }
}

/*
// 测试代码
const reg = Reg.getInstance();

// 添加一个注册表项
reg.add('HKCU\\Software\\Test', 'Value1', 'Data1')
  .then((output) => {
    console.log(output);
  })
  .catch((error) => {
    console.error(error);
  });

// 删除一个注册表项
reg.remove('HKCU\\Software\\Test', 'Value1')
  .then((output) => {
    console.log(output);
  })
  .catch((error) => {
    console.error(error);
  });

// 批量添加多个注册表项
const items: RegItem[] = [
  {
    key: 'HKCU\\Software\\Test',
    value: 'Value2',
    value_type: '',
    data: 'Data2',
  },
  {
    key: 'HKCU\\Software\\Test',
    value: 'Value3',
    value_type: '',
    data: 'Data3',
  },
  {
    key: 'HKCU\\Software\\Test2',
    value: 'Value4',
    value_type: '',
    data: 'Data4',
  },
];

reg.batchAdd(items)
  .then((output) => {
    console.log(output);
  })
  .catch((error) => {
    console.error(error);
  });
	
let bundle: { [key: string]: any } = {};
// win10
const winVersionData : WinVersion|undefined = winVersions["win10"];
bundle = {
	"HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows NT\\CurrentVersion": [
		{
			"value": "CSDVersion",
			"data": winVersionData["CSDVersion"],
		},
		{
			"value": "CurrentBuild",
			"data": winVersionData["CurrentBuild"],
		},
		{
			"value": "CurrentBuildNumber",
			"data": winVersionData["CurrentBuildNumber"],
		},
		{
			"value": "CurrentVersion",
			"data": winVersionData["CurrentVersion"],
		},
		{
			"value": "ProductName",
			"data": winVersionData["ProductName"],
		},
		{
			"value": "CurrentMinorVersionNumber",
			"data": winVersionData["CurrentMinorVersionNumber"],
			"key_type": "dword",
		},
		{
			"value": "CurrentMajorVersionNumber",
			"data": winVersionData["CurrentMajorVersionNumber"],
			"key_type": "dword",
		},
	],
	"HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\Windows": [
		{
			"value": "CSDVersion",
			"data": winVersionData["CSDVersionHex"],
			"key_type": "dword",
		},
	],
};

reg.importBundle(bundle)
  .then((output) => {
    console.log(output);
  })
  .catch((error) => {
    console.error(error);
  });

	*/
