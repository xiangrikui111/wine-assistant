/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import { WinVersion, winVersions } from "../commons/catalogs";
import { WinArchEnum } from "../commons/define";
import { WineCommand } from "./WineCommand";

/**
 * supported versions:
            - win10 (Microsoft Windows 10)
            - win81 (Microsoft Windows 8.1)
            - win8 (Microsoft Windows 8)
            - win7 (Microsoft Windows 7)
            - win2008r2 (Microsoft Windows 2008 R1)
            - win2008 (Microsoft Windows 2008)
            - winxp (Microsoft Windows XP)
 */
export function setWindows(
  version: string,
  arch: string = WinArchEnum.win64
): Promise<string> {
  return new Promise<string>(async (resolve, reject) => {
    const winVersionData: WinVersion | undefined = winVersions[version];
    if (!winVersionData) {
      reject("Given version is not supported.");
      return;
    }

    if (version === "winxp" && arch === WinArchEnum.win64) {
      version = "winxp64";
    }
    const wineCmd = new WineCommand(
      "winecfg.exe",
      { WINEDLLOVERRIDES: "mscoree=  mshtml=" },
      `/v ${version}`
    );

    wineCmd.run().then(resolve, reject);
  });
}
