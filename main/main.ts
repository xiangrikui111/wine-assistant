/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
// electron-main/index.ts
import { app, BrowserWindow, ipcMain, Menu, screen } from "electron";
import path from "path";
import { ProgramManager } from "./managers/ProgramManager";
import { DownloaderManager } from "./managers/DownloaderManager";
import { DependencyManager } from "./managers/DependencyManager";
import { ProgramInternal } from "./internal/ProgramInternal";
import { DependencyInternal } from "./internal/DependencyInternal";
import { IpcManager } from "./managers/IpcManager";
import { StatusManager } from "./managers/StatusManager";
import { Wineboot } from "./utils/Wineboot";
import { SysInfoManager } from "./managers/SysInfoManager";
import { logger } from "./utils/Logger";
import { exec } from "child_process";

const createWindow = (): BrowserWindow => {
  const win = new BrowserWindow({
    show: false,

    width: 1280,
    height: 720,
    minWidth: 1280,
    minHeight: 720,
    webPreferences: {
      contextIsolation: true, // 是否开启隔离上下文
      nodeIntegration: true, // 渲染进程使用Node API
      preload: path.join(__dirname, "./preload.js"), // 需要引用js文件
    },
  });

  // 如果打包了，渲染index.html
  if (app.isPackaged) {
    win.loadFile(path.join(__dirname, "./index.html"));
  } else {
    let url = "http://localhost:3000"; // 本地启动的vue项目路径
    win.loadURL(url).then(() => {});
  }
  win.maximize();
  win.show();
  return win;
};
const devMenu: Electron.MenuItemConstructorOptions[] = [
  {
    label: "View",
    submenu: [
      {
        label: "wine配置",
        click: () => {
          exec("bash wine.sh");
        },
      },
      {
        role: "reload",
      },
    ],
  },
  {
    role: "help",
    submenu: [
      {
        label: "wine助手使用说明",
        click: () => {
          let apiWin = new BrowserWindow({
            width: 500,
            height: 500,
            show: true,
          });
          apiWin.maximize();
          apiWin.show();
          apiWin.loadFile("./docs/index.html");
          apiWin.on("closed", () => {});
        },
      },
      {
        label: "Typedoc help",
        click: () => {
          let apiWin = new BrowserWindow({
            width: 500,
            height: 500,
            show: false,
          });
          apiWin.maximize();
          apiWin.show();
          apiWin.loadURL("https://typedoc.org/guides/tags/");
          apiWin.on("closed", () => {});
        },
      },
      {
        label: "Dev tools",
        click: () => {
          win.webContents.openDevTools();
        },
      },
    ],
  },
];
let win;

const gotTheLock = app.requestSingleInstanceLock();
if (!gotTheLock) {
  app.quit();
} else {
  app.on("second-instance", (event, commandLine, workingDirectory) => {
    // 当运行第二个实例时,将会聚焦到myWindow这个窗口
    if (win) {
      if (win.isMinimized()) win.restore();
      win.focus();
    }
  });
  if (app.isPackaged) {
    app.enableSandbox();
  }

  app.whenReady().then(() => {
    //console.log("xwtest-------------", screen.getPrimaryDisplay().workArea);
    win = createWindow(); // 创建窗口
    if (import.meta.env.DEV) {
      const m = Menu.buildFromTemplate(devMenu);
      Menu.setApplicationMenu(m);
      win.webContents.openDevTools();
    } else {
      Menu.setApplicationMenu(null);
    }

    let ipcManager = IpcManager.getInstance();
    ipcManager.regProcess("program", ProgramManager.getInstance());
    ipcManager.regProcess("dependency", DependencyManager.getInstance());
    ipcManager.regProcess("programInternal", new ProgramInternal());
    ipcManager.regProcess("downloaderManager", DownloaderManager.getInstance());
    ipcManager.regProcess("statManager", StatusManager.getInstance());
    ipcManager.regProcess("dependencyInternal", new DependencyInternal());
    ipcManager.regProcess("sysInfo", new SysInfoManager());
    ipcManager.startup();

    app.on("activate", () => {
      if (BrowserWindow.getAllWindows().length === 0) createWindow();
    });
    //initWineAssistant();
    Wineboot.checkAndInitContainer();
  });

  // 关闭窗口
  app.on("will-quit", () => {
    if (process.platform !== "darwin") {
      app.quit();
    }
    IpcManager.getInstance().shutdown();
  });
}
