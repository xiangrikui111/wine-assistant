/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import {
  Grade,
  GradeEnum,
  LinuxArchEnum,
  ProgramCategory,
  ProgramInfo,
  ProgramRun,
  WinArchEnum,
} from "../commons/define";
import { BaseManager } from "./Base";
import { Step } from "../commons/define";
import * as path from "path";

import {
  ProgramInstallStep,
  ProgramRunScriptStep,
} from "../internal/ProgramSetps";
import { ProgramExec } from "../internal/ProgramExec";
import { RelationManager } from "./RelationManager";
import { logger } from "../utils/Logger";
import { BrowserWindow, shell, net, dialog } from "electron";
import contextMenu from "electron-context-menu";

export class ProgramManager extends BaseManager {
  protected _repo: string = "win-program";
  private _programs: ProgramInfo[] = [] as ProgramInfo[];
  private static instance: ProgramManager;
  private constructor() {
    super();
  }
  /**
   * 获取ProgramManager实例
   * @returns ProgramManager
   * @example
   * ```ts
   * ProgramManager.getInstance()
   * ```
   */
  public static getInstance(): ProgramManager {
    if (!this.instance) {
      this.instance = new ProgramManager();
    }
    return this.instance;
  }

  /**
   * 获取应用运行信息
   *
   * @param category 应用分类
   * @param name 应用名称
   * @returns Promise<ProgramRun>
   * @example
   * ```ts
   * ProgramManager.getInstance().getProgramRun('wechat').then(result=>{
   *   // do something
   * }, err=>{
   *   // do error
   * })
   * ```
   */
  public getProgramRun(category: ProgramCategory, key: string): Promise<any> {
    return new Promise<any>((res, rej) => {
      if (key == "") {
        res(undefined);
        return;
      }
      //let configPath = `${category}/${key}`
      let filepath = `${category}/${key}.yml`;
      this.loadConfig(filepath).then(
        (data) => {
          let steps: Step[] = [] as Step[];
          let programRun: ProgramRun = Object.assign(new ProgramRun(), data);
          programRun.Executable = Object.assign(
            new ProgramExec(),
            data["Executable"]
          );
          data["Steps"].forEach((s, _i) => {
            switch (s.action) {
              case "install":
                steps.push(Object.assign(new ProgramInstallStep(), s));
                break;
              case "run_script":
                steps.push(Object.assign(new ProgramRunScriptStep(), s));
                break;
            }
          });
          programRun.Key = key;
          programRun.Steps = steps;
          res(programRun);
        },
        (err) => {
          logger.error("on program run", err);
          rej(err);
        }
      );
    });
  }

  /**
   * 获取应用列表
   * @param pageno 分页页码，页码从1开始计数
   * @param pagesize 分页大小
   * @returns Promise<ProgramInfo[]>
   * @example
   * ```ts
   * ProgramManager.getInstance().getPrograms(1,10).then(result=>{
   *   // do something
   * }, err=>{
   *   // do error
   * })
   * ```
   */
  public getPrograms(): Promise<ProgramInfo[]> {
    return new Promise<ProgramInfo[]>((res, rej) => {
      if (this._programs.length == 0) {
        this.loadConfig("index.yml").then(
          (data) => {
            for (let k in data) {
              let program: ProgramInfo = Object.assign(
                new ProgramInfo(),
                data[k]
              );
              program.Key = k;

              if (!GradeEnum[program.Grade]) {
                logger.error("Grade invalid:", k, ",", program.Grade);
                continue;
              }
              let invalidArch = [] as string[];
              program.LinuxArch.forEach((arch) => {
                if (!LinuxArchEnum[arch]) {
                  invalidArch.push(arch);
                }
              });
              if (invalidArch.length > 0) {
                logger.error("LinuxArch invalid:", k, ",", invalidArch);
                continue;
              }
              if (!WinArchEnum[program.Arch]) {
                logger.error("WinArch invalid:", k, ",", program.Arch);
                continue;
              }

              if (
                program.Publish === false &&
                process.env.WINE_ASSISTANT_DEBUG != "true"
              ) {
                continue;
              }
              this._programs.push(program);
            }
            this._programs.sort((a, b) => {
              const codeFunc = (grade: Grade): number => {
                switch (grade) {
                  case GradeEnum.Gold:
                    return 4;
                  case GradeEnum.Silver:
                    return 3;
                  case GradeEnum.Bronze:
                    return 2;
                  case GradeEnum.Fail:
                    return -1;
                  default:
                    return 0;
                }
              };
              return codeFunc(b.Grade) - codeFunc(a.Grade);
            });
            res(this._programs);
          },
          (err) => {
            rej(err);
          }
        );
      } else {
        res(this._programs);
      }
    });
  }
  public ipcProcess(action: string, data: any): Promise<any> {
    //throw new Error("Method not implemented.");
    return new Promise<any>((resolve, reject) => {
      switch (action) {
        case "openDetail":
          this.getProgramRun(data.Category, data.Key).then(
            (run: ProgramRun) => {
              let downUrl = "";
              for (let i = 0; i < run.Steps.length; i++) {
                if (run.Steps[i].action == "install") {
                  downUrl = (run.Steps[i] as ProgramInstallStep).url;
                  break;
                }
              }
              this.openDetail(run.DetailUrl, downUrl, run.Name).then(
                (url) => {
                  if (url != "") {
                  }
                  resolve(url);
                },
                (reason) => {
                  reject(reason);
                }
              );
            },
            (reason) => {
              reject(reason);
            }
          );
          break;
        case "getPrograms":
          this.getPrograms().then(
            (res) => {
              resolve(res);
            },
            (err) => {
              reject(err);
            }
          );
          break;
        case "getDependencies":
          this.getProgramRun(data.Category, data.Key).then(
            (run: ProgramRun) => {
              if (
                run == undefined ||
                run.Dependencies == null ||
                run.Dependencies.length == 0
              ) {
                resolve([]);
              } else {
                RelationManager.getInstance().recursiveDependencies(
                  data.Key,
                  run,
                  1,
                  (deps, err: string): void => {
                    if (err != "") {
                      logger.error(err);
                      reject("检查依赖关系失败");
                    } else {
                      resolve(deps);
                    }
                  },
                  (reason) => {
                    reject(reason);
                  }
                );
              }
            },
            (reason) => {
              reject(reason);
            }
          );
          break;
        default:
          reject("Unsupport action");
      }
    });
  }

  public openDetail(
    detail: string,
    downUrl: string,
    pName: string
  ): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      contextMenu({
        showSelectAll: false,
        showSaveLinkAs: true,
        showInspectElement: false,
      });
      let detailWin = new BrowserWindow({
        width: 1280,
        height: 720,
        show: false,
        webPreferences: {
          contextIsolation: true, // 是否开启隔离上下文
          nodeIntegration: true, // 渲染进程使用Node API
          //nativeWindowOpen: false,
          //preload: path.join(__dirname, "./detail.js"), // 需要引用js文件
        },
      });
      detailWin.show();
      detailWin.loadURL(detail);
      //detailWin.webContents.session.protocol.uninterceptProtocol("https");
      detailWin.webContents.on("dom-ready", (ev) => {
        /*
        detailWin.webContents.session.protocol.interceptStringProtocol(
          "https",
          (request, callback) => {
            net.request(request.url).on("response",resp=>{
              callback(resp.)
            })
            logger.debug("intercept", request.url);
            callback({
              url: request.url,
              referrer: request.referrer,
              session: undefined,
            });
          }
        );
         */
      });
      //let downloadUrl = "";
      detailWin.webContents.on("will-navigate", (ev, url) => {
        if (url == downUrl) {
          //detailWin.webContents.session.protocol.uninterceptProtocol("https");
          logger.debug("will navigate", url);
        } else {
          ev.preventDefault();
        //  logger.debug("prevent default", url);
          dialog.showMessageBox(detailWin, {
            title: "提示:",
            message: `检测到您点的链接可能不是'普通下载'按钮链接，进入其他页面可能会导致软件异常，是否继续访问？`,
            buttons: ["是", "否"],
          })
          .then((val) => {
            if (val.response == 0) {

              detailWin.webContents.loadURL(url);
              /*if (!detailWin.isDestroyed()) {
                //detailWin.webContents.removeAllListeners();
                detailWin.close();
                //detailWin.destroy();
              } */
            } else {
              
            }
          });
          
        }
      });
      detailWin.webContents.on("did-navigate", (ev, url, code, text) => {
        if (code >= 400) {
          reject(`${code}: ${text}`);
        }
      });
      if (import.meta.env.DEV) {
        detailWin.webContents.openDevTools();
      }
      /*
      detailWin.webContents.on("new-window", (ev) => {
        ev.preventDefault();
      });
      */
      detailWin.webContents.setWindowOpenHandler((detail) => {
        detailWin.webContents.session.downloadURL(detail.url);
        return { action: "deny" };
      });
      detailWin.webContents.on("destroyed", () => {
        logger.debug("on destoryed ");
        resolve(appUrl);
      });
      detailWin.on("closed", () => {
        logger.debug("on closed");
      });
      let appUrl = "";
      detailWin.webContents.session.removeAllListeners();
      detailWin.webContents.session.on(
        "will-download",
        (event, item, contents) => {
          item.setSavePath("/tmp/" + item.getFilename());
          item.cancel();
          logger.debug(item.getURLChain());
          /*
          let itemUrlObj = new URL(item.getURL());
          let downUrlObj = new URL(downUrl);
          let itemFileName = path.basename(itemUrlObj.pathname);
          let downFileName = path.basename(downUrlObj.pathname);
          */
          //logger.debug(item.getURL(), itemFileName, downFileName);
          if (item.getURLChain()[0] == downUrl) {
            dialog
              .showMessageBox(detailWin, {
                title: "提示:",
                message: `是否下载‘${pName}’，并使用Wine助手进行安装？`,
                buttons: ["是", "否"],
              })
              .then((val) => {
                if (val.response == 0) {
                  appUrl = item.getURL();
                  if (!detailWin.isDestroyed()) {
                    detailWin.close();
                    //detailWin.destroy();
                  } 
                } else {

                }
              }); 
            /*appUrl = item.getURL();
            if (!detailWin.isDestroyed()) {
              detailWin.close();
            } */
          } else {
            dialog
              .showMessageBox(detailWin, {
                title: "提示:",
                message: `检测到您点击的并非‘${pName}’的下载链接或者链接可能已更新，原始软件配置仓库的链接为‘${downUrl}’ 点击的新下载链接为 ‘${item.getURL()}’请选择下载链接并使用wine助手安装`,
                buttons: ["新下载链接", "原始配置链接","取消"],
                cancelId:2
              })
              .then((val) => {
                if (val.response == 0) {
                  logger.debug('第一个选项');
                  appUrl = item.getURL();
                  if (!detailWin.isDestroyed()) {
                    detailWin.close();
                    //detailWin.destroy();
                  } 
                } else if (val.response == 1){
                  logger.debug('第二个选项');
                  appUrl = downUrl;
                  if (!detailWin.isDestroyed()) {
                    detailWin.close();
                    //detailWin.destroy();
                  } 
                } else{
                  logger.debug('用户关闭了消息框');
                } 
              }); 
          }
        }
      );
    });
  }

  public distory(): void {}
}
