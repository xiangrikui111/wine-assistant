/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import * as fs from "fs";
import * as path from "path";
import * as mime from "mime";

import {
  DownloadStatus,
  ActionEnum,
  IpcProcess,
  DownloadFile,
} from "../commons/define";
import { WorkDirConfig } from "../commons/config";
import { logger } from "../utils/Logger";
import { IpcManager } from "./IpcManager";
import axios, {
  AxiosRequestConfig,
  CancelTokenSource,
  AxiosResponse,
} from "axios";
import { Crypto, deleteFileOrLink } from "../utils/Utils";
import { StatusManager } from "./StatusManager";

export class FileInfo implements DownloadFile {
  private _appKey: string = "";
  private _url: string = "";
  private _total: number = 0.0;
  private _size: number = 0.0;
  private _supportResume: boolean = true;
  private _status: DownloadStatus = DownloadStatus.Undownload;
  private _cancel?: CancelTokenSource;
  constructor(appKey: string, url: string) {
    this._url = url;
    this._appKey = appKey;
    this.syncFileSize();
  }
  get AppKey(): string {
    return this._appKey;
  }

  public syncFileSize() {
    if (this.DownloadStatus == DownloadStatus.DownloadPaused) {
      if (this._supportResume === true) {
        let fStat: fs.Stats = fs.statSync(this.DownloadingPath);
        if (fStat !== undefined) {
          this._size = fStat.size;
        }
      } else {
        this._size = 0;
      }
    } else if (
      this.DownloadStatus == DownloadStatus.Downloaded ||
      this._size > 0
    ) {
      if (!fs.existsSync(this.DownloadedPath)) {
        this._status = DownloadStatus.Undownload;
        this._size = 0;
      } else {
        this.DownloadStatus = DownloadStatus.Downloaded;
      }
    }
  }

  get DownloadStatus(): DownloadStatus {
    return this._status;
  }

  get FileKey(): string {
    return Crypto.md5(this._url);
  }
  get FileName(): string {
    try {
      let url = new URL(this._url);
      return path.basename(url.pathname);
    } catch (error) {
      return path.basename(this._url);
    }
  }
  get DownloadingPath(): string {
    return path.join(
      WorkDirConfig.DownloadingDir,
      this.FileKey + path.extname(this.FileName)
    );
  }
  get MimeType(): string {
    if (!fs.existsSync(this.DownloadedPath)) {
      return "";
    }
    return mime.getType(this.DownloadedPath);
  }
  get ExtName(): string {
    if (!fs.existsSync(this.DownloadedPath)) {
      return "";
    }
    /*
    fileType.fromFile(this.DownloadedPath).then((type) => {
      logger.debug(this.DownloadedPath, type);
    });
    */

    return path.extname(this.DownloadedPath).replaceAll(".", "");
    //return "";
  }
  get IsArchive(): boolean {
    return ["zip", "rar", "7z", "cab", "gz", "xz", "bz2"].includes(
      this.ExtName
    );
  }
  get DownloadedPath(): string {
    return path.join(
      WorkDirConfig.DownloadedDir,
      `${this._appKey}.${this.FileKey}${path.extname(this.FileName)}`
    );
  }
  get ProtableInstallPath(): string {
    return path.join(
      WorkDirConfig.ProtableDir,
      `${this._appKey}.${this.FileKey}`
      //Crypto.md5(this._url) + path.extname(this._url) + ".download"
    );
  }
  get Progress(): number {
    if (this._total == 0) {
      return 0;
    }
    return Math.floor((this._size / this._total) * 100 * 100) / 100; //Number.parseFloat(((this.Size / this.Total) * 100).toFixed(2));
  }
  set DownloadStatus(newStatus: DownloadStatus) {
    let oldStatus = this._status;
    this._status = newStatus;
    IpcManager.sendToView("installStatus", {
      ChangedStatus: "DownloadStatus",
      OldStatus: oldStatus,
      NewStatus: newStatus,
      FileKey: this.FileKey,
      Key: this._appKey,
    });
  }
  public pause() {
    if (this._status == DownloadStatus.Downloading) {
      this._cancel?.cancel();
      this.DownloadStatus = DownloadStatus.DownloadPaused;
    }
  }
  public remove() {
    if (this._status == DownloadStatus.Downloading) {
      return;
    }

    if (fs.existsSync(this.DownloadedPath)) {
      fs.unlinkSync(this.DownloadedPath); // 删除下载文件
    }
    if (fs.existsSync(this.DownloadingPath)) {
      fs.unlinkSync(this.DownloadingPath); // 删除下载文件
    }
    //this.updateStatus(DownloadStatus.Undownload);
    this.DownloadStatus = DownloadStatus.Undownload;
  }
  public cancel() {
    this.pause();
    this.remove();
  }
  fetchData(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      if (this._total !== 0 && this._size === this._total) {
        if (this.DownloadStatus !== DownloadStatus.Downloaded) {
          this.DownloadStatus = DownloadStatus.Downloaded;
        }
        resolve();
        return;
      }
      if (this._status === DownloadStatus.Downloading) {
        reject("Is downloading");
        return;
      }
      this._cancel = axios.CancelToken.source();
      if (this._size > this._total) {
        reject("File error");
        return;
      }
      const options: AxiosRequestConfig = {
        url: this._url,
        method: "GET",
        responseType: "stream",
        headers: {
          Range: `bytes=${this._size}-`,
        },
        cancelToken: this._cancel!.token,
      };
      this.DownloadStatus = DownloadStatus.Downloading;
      axios(options)
        .then((response: AxiosResponse) => {
          if (response.status > 300) {
            logger.error(
              this._appKey,
              this.FileName,
              response.status,
              response.statusText
            );
            reject(`${response.status} ${response.statusText}`);
            return;
          }
          let contentLength = parseInt(
            response.headers["content-length"] || "0"
          );
          //如果返回206表示服务器支持断点续传
          if (response.status == 206) {
            this._total = contentLength + this._size;
          } else {
            //服务器不支持断点续传
            this._total = contentLength;
            this._supportResume = false;
            this._size = 0;
            deleteFileOrLink(this.DownloadingPath);
          }
          //logger.debug(response.status, response.statusText, response.headers);

          const fileWriter = fs.createWriteStream(this.DownloadingPath, {
            flags: this._size > 0 ? "a+" : "w",
          });
          const startSize = this._size;
          const onData = (_: any) => {
            this._size = fileWriter.bytesWritten + startSize; // 已经下载的大小
            const progressBar: string = `${this._appKey}++++++++++++${this._size}/${this._total}/${fileWriter.bytesWritten}/${contentLength}++++++++++++`;
            process.stdout.write(`\r[${progressBar}] ${this.Progress}%`); // 显示进度条
          };
          const onErr = (err: Error) => {
            this._status = DownloadStatus.DownloadPaused;
            reject(err.message);
          };
          const onFinish = () => {
            this._size = fileWriter.bytesWritten + startSize;
            fileWriter.close();
            if (fs.existsSync(this.DownloadedPath)) {
              fs.unlinkSync(this.DownloadedPath);
            }
            fs.rename(this.DownloadingPath, this.DownloadedPath, (err) => {
              if (err) {
                logger.error("Rename fail: ", err.message);
                reject(err.message);
              } else {
                this.DownloadStatus = DownloadStatus.Downloaded;
                resolve();
              }
            });
          };
          fileWriter.on("error", onErr);
          fileWriter.on("finish", onFinish);
          response.data.on("data", onData);
          response.data.pipe(fileWriter);
        })
        .catch((reason) => {
          logger.error("Request fail: ", reason);
          reject(reason);
        });
    });
  }
}

export class DownloaderManager extends IpcProcess {
  protected _repo: string = "";
  private _taskConfig: { [key: string]: FileInfo } = {};
  //private _loaded: boolean = false;
  private static instance: DownloaderManager;
  private constructor() {
    super();
    this.loadConfig();
  }

  /**
   * 加载下载任务的配置信息
   */
  private loadConfig() {
    try {
      if (fs.existsSync(WorkDirConfig.DownloadStatusConfig)) {
        const data = fs.readFileSync(
          WorkDirConfig.DownloadStatusConfig,
          "utf8"
        );
        //this._taskConfig = JSON.parse(data);
        let obj = JSON.parse(data);
        Object.entries<[string, FileInfo]>(obj).forEach((val) => {
          //logger.debug(val);
          let fileKey = val[0];
          let fileInfo = Object.assign(
            new FileInfo(val[1]["_appKey"], val[1]["_url"]),
            val[1]
          );
          fileInfo.syncFileSize();
          this._taskConfig[fileKey] = fileInfo;
        });
      }
    } catch (error) {
      this._taskConfig = {};
    }
    //if (import.meta.env.DEV) this._loaded = true;
  }

  /**
   * 保存下载任务的配置信息
   */
  /*
   */
  private saveConfig() {
    //logger.debug("on save config", this._loaded);
    try {
      //if (import.meta.env.DEV) if (!this._loaded) return;

      //logger.debug(JSON.stringify(this._taskConfig));
      fs.writeFileSync(
        WorkDirConfig.DownloadStatusConfig,
        JSON.stringify(this._taskConfig, (key: string, value: any) => {
          if (key === "_cancel") {
            return undefined; // 忽略
          }
          if (key == "_status" && value == DownloadStatus.Downloading) {
            return DownloadStatus.DownloadPaused;
          }
          return value;
        })
      );
    } catch (error) {
      logger.error(error);
    }
  }
  /**
   * 获取DownloaderManager实例
   * @returns DownloaderManager
   * @example
   * ```ts
   * DownloaderManager.getInstance()
   * ```
   */
  public static getInstance(): DownloaderManager {
    if (!this.instance) {
      this.instance = new DownloaderManager();
    }
    return this.instance;
  }

  public getFileInfo(key: string): FileInfo | undefined {
    return this._taskConfig[key];
  }
  public getFileInfoByUrl(url: string): FileInfo | undefined {
    return this.getFileInfo(Crypto.md5(url));
  }
  public start(appKey: string, url: string): Promise<FileInfo | undefined> {
    return new Promise<FileInfo | undefined>((resolve, reject) => {
      let fileInfo = this.getFileInfoByUrl(url);
      if (fileInfo == undefined) {
        fileInfo = new FileInfo(appKey, url);
        this._taskConfig[fileInfo.FileKey] = fileInfo;
      }
      logger.debug("on start", fileInfo);
      if (fileInfo.DownloadStatus === DownloadStatus.Downloading) {
        reject(fileInfo.DownloadStatus);
        return;
      }
      if (fileInfo.DownloadStatus === DownloadStatus.Downloaded) {
        resolve(fileInfo);
        return;
      }

      StatusManager.getInstance().addDownloadFile(
        appKey,
        fileInfo!.FileKey,
        fileInfo!.FileName
      );
      fileInfo
        .fetchData()
        .then(() => {
          resolve(fileInfo);
        })
        .catch((reason) => {
          StatusManager.getInstance().removeDownloadFile(
            appKey,
            fileInfo!.FileKey
          );
          delete this._taskConfig[fileInfo!.FileKey];
          reject(reason);
        });
    });
  }
  public resume(fileKey: string): Promise<FileInfo | undefined> {
    return new Promise<FileInfo | undefined>((resolve, reject) => {
      let fileInfo = this.getFileInfo(fileKey);
      if (fileInfo === undefined) {
        reject("File not found");
        return;
      }
      if (fileInfo.DownloadStatus === DownloadStatus.Downloading) {
        reject(fileInfo.DownloadStatus);
        return;
      }
      if (fileInfo.DownloadStatus === DownloadStatus.Downloaded) {
        resolve(fileInfo);
        return;
      }

      fileInfo
        .fetchData()
        .then(() => {
          resolve(fileInfo);
        })
        .catch((reason) => {
          StatusManager.getInstance().removeDownloadFile(
            fileInfo!.AppKey,
            fileInfo!.FileKey
          );
          delete this._taskConfig[fileInfo!.FileKey];
          reject(reason);
        });
    });
  }

  public pause(fileKey: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      let fileInfo = this.getFileInfo(fileKey);
      if (fileInfo === undefined) {
        reject("File not found");
        return;
      }
      fileInfo.pause();
      resolve("ok");
    });
  }
  public cancel(fileKey: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      let fileInfo = this.getFileInfo(fileKey);
      if (fileInfo === undefined) {
        reject("File not found");
        return;
      }
      fileInfo.cancel();
      delete this._taskConfig[fileKey];
      resolve("ok");
    });
  }

  /**
   * ipcMain将调用此函数处理前段的请求, 实现逻辑同Manager
   * @param contents
   * @param action
   * @param programName
   */
  public ipcProcess(action: string, param: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (action == "opDownload") {
        let p: Promise<any> | undefined = undefined;
        switch (param.Action) {
          case ActionEnum.Resume:
            p = this.resume(param.fileKey);
            break;
          case ActionEnum.Pause:
            p = this.pause(param.fileKey);
            break;
          case ActionEnum.Delete:
            p = this.cancel(param.fileKey);
            break;
        }
        if (p == undefined) {
          reject("Unsupport operation");
        } else {
          p.then((val) => {
            resolve(val);
          }).catch((reason) => {
            reject(reason);
          });
        }
      }
      if (action == "getProgress") {
        resolve(this.getProgress(param as string[]));
      } else {
        reject("Unsupported action");
      }
    });
  }

  public getProgress(fileKeys?: string[]): Object {
    let result = {};
    if (fileKeys !== undefined && fileKeys.length > 0) {
      Object.entries(this._taskConfig).forEach((val) => {
        if (fileKeys.indexOf(val[0]) >= 0) {
          let progress = {} as DownloadFile;
          progress.DownloadStatus = val[1].DownloadStatus;
          progress.FileKey = val[1].FileKey;
          progress.FileName = val[1].FileName;
          progress.Progress = val[1].Progress;
          result[val[0]] = progress;
        }
      });
    } else {
      Object.entries(this._taskConfig).forEach((val) => {
        let progress = {} as DownloadFile;
        progress.DownloadStatus = val[1].DownloadStatus;
        progress.FileKey = val[1].FileKey;
        progress.FileName = val[1].FileName;
        progress.Progress = val[1].Progress;
        result[val[0]] = progress;
      });
    }
    return result;
  }

  public distory(): void {
    this.saveConfig();
  }
}
