/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import chokidar from "chokidar";
import { WorkDirConfig } from "../commons/config";
import {
  UninstallInfo,
  InstallInfo,
  InstallStatus,
  IpcProcess,
} from "../commons/define";
import { removeDesktop, loadConfig, saveConfig } from "../utils/Utils";
import path from "path";
import fs from "fs";
import { IpcManager } from "./IpcManager";
import { logger } from "../utils/Logger";
import { Uninstaller } from "../utils/Uninstaller";

export class StatusManager extends IpcProcess {
  private static _instance: StatusManager | undefined = undefined;
  //  private _fileStatus: Map<string, FileStatus>;
  private _installStatus: { [key: string]: InstallInfo } = {};
  //private _downloadStatus: { [key: string]: DownloadFile } = {};
  private _addFileToKey: { [key: string]: string } = {};
  private _uninstalls: { [key: string]: string } = {};
  private _unlinkFileToKey: { [key: string]: string } = {};
  //  private _loaded: boolean = false;
  private _execWatcher: chokidar.FSWatcher;
  private _watchDirs: string[] = [
    path.join(
      WorkDirConfig.ContainerDir,
      WorkDirConfig.ContainerName,
      "drive_c"
    ),
  ];

  private constructor() {
    super();
    this.init();

    //setTimeout(this.doUninstallClean,2000)
    this.doUninstallClean();
    // 检测目录是否存在，不存在则创建
    this._watchDirs.forEach((dirPath: string) => {
      if (!fs.existsSync(dirPath)) {
        try {
          fs.mkdirSync(dirPath, { recursive: true });
        } catch (error: any) {
          logger.error(`创建目录失败：${dirPath}，错误信息：${error.message}`);
        }
      }
    });
    this._execWatcher = chokidar.watch(this._watchDirs, {
      //followSymlinks: false,
      ignored: ["**/Temp/**/*"],
      depth: 10,
    });
    const newFileCallback = (file, stats) => {
      if (!stats?.isFile()) {
        return;
      }

      //获取文件名
      let filename = path.basename(file);
      let ext = path.extname(file);
      //检查是否为监听的文件
      let key = this._addFileToKey[filename];
      if (key !== undefined) {
        //是监听的文件，获取文件状态
        let fStatus = this._installStatus[key];
        if (fStatus !== undefined) {
          logger.debug(`watched file ${file}, key ${key}`);
          //获取到文件状态，更新ExecFilePath和InstallStatus
          let oldPath = fStatus.ExecFilePath;
          fStatus.ExecFilePath = file;
          this.updateInstallStatus(key, InstallStatus.Installed);
          this._installStatus[key] = fStatus;
          if (oldPath && oldPath !== "") {
            delete this._unlinkFileToKey[oldPath];
          }
          //将全路径添加到删除文件监听列表中
          this._unlinkFileToKey[file] = key;
        }
      } else if (ext.toLowerCase() == ".lnk") {
        //logger.debug("xwtest-----------------------", file);
        fs.unlinkSync(file);
      }
    };
    this._execWatcher.on("add", newFileCallback);
    this._execWatcher.on("unlink", (file) => {
      //获取文件名
      //let filename = path.parse(file).base;
      //检查是否为监听的文件
      let key = this._unlinkFileToKey[file];
      if (key !== undefined) {
        //是监听的文件，获取文件状态
        let fStatus = this._installStatus[key];
        if (fStatus !== undefined) {
          logger.debug(`unlinked file ${file}, key ${key}`);
          this.updateInstallStatus(key, InstallStatus.Uninstall);
          fStatus.ExecFilePath = undefined;
          this._installStatus[key] = fStatus;
          //delete this._unlinkFileToKey[file];
        }
      }
    });
  }
  private checkInstalStatus() {
    for (const key in this._installStatus) {
      const installStatus = this._installStatus[key];
      if (installStatus.ExecFilePath !== undefined) {
        if (
          installStatus.InstallStatus == InstallStatus.Installed &&
          !fs.existsSync(installStatus.ExecFilePath)
        ) {
          installStatus.InstallStatus = InstallStatus.Uninstall;
        } else if (
          installStatus.InstallStatus == InstallStatus.Uninstall &&
          fs.existsSync(installStatus.ExecFilePath)
        ) {
          installStatus.InstallStatus = InstallStatus.Installed;
        }
        if (installStatus.InstallStatus == InstallStatus.Installed) {
          this._unlinkFileToKey[installStatus.ExecFilePath] = key;
        }
        this._installStatus[key] = installStatus;
      }
    }
  }
  private init() {
    let data = loadConfig(WorkDirConfig.InstallStatusConfig);
    if (data != "") {
      let obj = JSON.parse(data);
      this._installStatus = Object.assign(this._installStatus, obj);
      this.checkInstalStatus();
    }
  }
  public static getInstance() {
    if (!this._instance) {
      this._instance = new StatusManager();
    }
    return this._instance;
  }
  async ipcProcess(action: string, param: any): Promise<any> {
    //throw new Error("Method not implemented.");
    switch (action) {
      case "getInstallStatus":
        return Promise.resolve(this.getInstallInfos(param as string[]));
      default:
        return Promise.reject("Unsupported action");
    }
  }
  /*
  public updateIsProtable(key: string, isProtable: boolean) {
    if (this._installStatus[key] == undefined) {
      return false;
    }
    this._installStatus[key].IsProtable = isProtable;
  }
  */
  public updateUninstallInfo(
    key: string,
    uninstallKey: string,
    displayName: string,
    desktopName: string,
    isProtable: boolean = false
  ) {
    if (this._installStatus[key] == undefined) {
      return false;
    }
    var uninstaller: UninstallInfo = {
      UninstallKey: uninstallKey, //如果IsProtable为true，值为安装路径，否则为guid
      DesktopName: desktopName,
      DisplayName: displayName,
    };

    this._installStatus[key].UninstallInfo = uninstaller;
    this._installStatus[key].IsProtable = isProtable;
  }
  public addWatch(key: string, filename: string): boolean {
    logger.debug("on add watch", key, filename);
    if (this._installStatus[key] == undefined) {
      return false;
    }
    //this._fileStatus[key].WatchFile = filename;
    this._addFileToKey[filename] = key;
    return true;
  }

  public addUninstall(key: string, name: string) {
    if (this._installStatus[key] !== undefined) {
      this._uninstalls[key] = name;
    }
  }
  public removeInstallStatus(key: string) {
    let fStatus = this._installStatus[key];
    if (fStatus !== undefined) {
      delete this._installStatus[key];
      if (fStatus !== undefined) {
        delete this._addFileToKey[fStatus.ExecFilePath!];
        delete this._unlinkFileToKey[fStatus.ExecFilePath!];
      }
    }
  }

  public addDownloadFile(key: string, fileKey: string, fileName: string) {
    if (this._installStatus[key] !== undefined) {
      this._installStatus[key].DownloadFiles[fileKey] = fileName;
    }
  }
  public removeDownloadFile(key: string, fileKey: string) {
    if (this._installStatus[key] !== undefined) {
      delete this._installStatus[key].DownloadFiles[fileKey];
    }
  }

  /**
   * 更新文件状态
   * @param key 文件key
   * @param status 状态
   * @param progress? 下载进度, Undownload值为0，Downloaded值为100，其它情况值为(0,100)
   * @returns
   */
  public updateInstallStatus(
    key: string,
    status: InstallStatus,
    execPath?: string
    //progress?: number
  ) {
    //let fStatus = this._fileStatus[key];
    if (this._installStatus[key] == undefined) {
      this._installStatus[key] = {
        //DownloadStatus: DownloadStatus.Undownload,
        InstallStatus: InstallStatus.Uninstall,
        DownloadFiles: {},
        //DownloadProgress: 0,
        ExecFilePath: undefined,
        IsProtable: false,
        UninstallInfo: { UninstallKey: "", DisplayName: "", DesktopName: "" },
      } as InstallInfo;
    }
    let oldStatus = this._installStatus[key].InstallStatus;
    if (oldStatus !== status) {
      IpcManager.sendToView("installStatus", {
        ChangedStatus: "InstallStatus",
        OldStatus: oldStatus,
        NewStatus: status,
        Key: key,
      });
      this._installStatus[key].InstallStatus = status;
      if (status === InstallStatus.Installed) {
        this.persistentStatus();
      }
    }
    if (status === InstallStatus.Installed && execPath !== undefined) {
      this._installStatus[key].ExecFilePath = execPath;
    }
  }
  //处理正常逻辑卸载的应用，即uninstall键被删除的应用
  private doUninstallClean() {
    //logger.debug(this._uninstalls);
    if (Object.entries(this._uninstalls).length == 0) {
      setTimeout(() => {
        this.doUninstallClean();
      }, 2000);
      return;
    }

    let uninstaller = new Uninstaller();
    uninstaller.list().then((uninstalls) => {
      Object.entries(this._uninstalls).forEach((item) => {
        let fStatus = this._installStatus[item[0]];
        logger.debug(this._uninstalls);
        //logger.debug("xwtest---------------", uninstalls, item, fStatus);
        if (
          fStatus !== undefined &&
          fStatus.IsProtable === false &&
          uninstalls[fStatus.UninstallInfo.UninstallKey] === undefined
        ) {
          removeDesktop(fStatus.UninstallInfo.DesktopName);
          delete this._uninstalls[item[0]];
          //delete this._installStatus[item[0]];
          //this.removeFileStatus(item[0]);
        }
      });
      setTimeout(() => this.doUninstallClean(), 2000);
    });
  }
  //处理异常逻辑卸载的应用，即命令行删除的应用
  public isInstalled(key: string): boolean {
    let info = this.getInstallInfo(key);
    if (info && info.InstallStatus == InstallStatus.Installed) {
      return true;
    } else {
      return false;
    }
  }
  public getInstallInfo(key: string): InstallInfo | undefined {
    return this._installStatus[key];
  }

  public getInstallInfos(keys?: string[]): Object {
    if (keys !== undefined && keys.length > 0) {
      let r = {};
      Object.entries(this._installStatus).forEach((val) => {
        if (keys.indexOf(val[0]) >= 0) {
          r[val[0]] = val[1];
        }
      });
      return r;
    } else {
      return this._installStatus;
    }
  }
  private persistentStatus() {
    saveConfig(
      WorkDirConfig.InstallStatusConfig,
      JSON.stringify(this._installStatus, (key: string, value: any) => {
        if (key === "InstallStatus" && value === "Installing") {
          return "Uninstall";
        }
        return value;
      })
    );
  }
  distory() {
    this._execWatcher.close();
    this.persistentStatus();
  }
}
