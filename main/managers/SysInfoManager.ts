/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import { ContainerStatus, IpcProcess, SysInfo } from "../commons/define";
import { Wineboot } from "../utils/Wineboot";

export class SysInfoManager extends IpcProcess {
  ipcProcess(action: string, _param: any): Promise<any> {
    //throw new Error("Method not implemented.");
    return new Promise<any>((res, _rej) => {
      switch (action) {
        case "getSysInfo":
          let result = {} as SysInfo;
          result.ContainerStatus = Wineboot.IsInitialized()
            ? ContainerStatus.Initialized
            : ContainerStatus.UnInitialization;
          res(result);
      }
    });
  }
}
