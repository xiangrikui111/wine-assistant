/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import path from "path";
import axios from "axios";
import fs from "fs";
import { IpcProcess, WineAssistantConfig } from "../commons/define";
import { Crypto } from "../utils/Utils";
import * as yaml from "yaml";
import { WorkDirConfig } from "../commons/config";
import { logger } from "../utils/Logger";

class Release {
  Branch: string = "";
}
/**
 * 所有Manager抽象基类，负责定义统一的方法
 */
export abstract class BaseManager extends IpcProcess {
  /**
   * 定义git数据源根路径
   */
  private _root: string = "https://gitee.com/openkylin/";
  private _repoTemplate =
    "https://gitee.com/openkylin/#REPO#/raw/#BRANCH#/#FILEPATH#";
  /**
   * 本地缓存根路径
   */
  private _localRoot: string = WorkDirConfig.WorkDir; //path.join(process.env.HOME!, ".wine-assistant");
  private _memoryCache: { [key: string]: string } = {};

  private loadWineAssistantConfig() {
    let cfgPath = path.join(WorkDirConfig.ConfigDir, "wine_assistant.json");
    let cfg: WineAssistantConfig;
    if (fs.existsSync(cfgPath)) {
      cfg = require(cfgPath);
      if (cfg.RepoTemplate && cfg.RepoTemplate !== "") {
        this._repoTemplate = cfg.RepoTemplate;
      }
    }
  }
  constructor() {
    //const cfg =
    super();
    this.loadWineAssistantConfig();
  }
  /*
  private _branch: string = `raw/${
    process.env.WINE_ASSISTANT_BRANCH || "master"
  }`;
  */
  /**
   * 定义仓库地址，由每个子类定义
   */
  protected abstract _repo: string;

  /**
   * 从本地缓存加载配置文件
   * @param filepath 路径
   * @returns
   */
  protected loadFromLocal(filepath: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      resolve("");
      return;
      let localPath = path.join(this._localRoot, this._repo, filepath);
      let basePath = path.parse(localPath);

      if (!fs.existsSync(basePath.dir)) {
        let err = fs.mkdirSync(basePath.dir, { recursive: true });
        if (err != undefined) {
          reject(err);
        }
      }
      if (!fs.existsSync(localPath)) {
        resolve("");
      } else {
        fs.readFile(localPath, {}, (err, data) => {
          if (err != null) {
            reject(err);
          } else {
            resolve(data.toString());
          }
        });
      }
    });
  }

  /**
   * 将远端的配置文件保存到本地缓存
   * @param filepath 文件路径
   * @param data 配置文件数据
   * @returns
   */
  protected saveToLocal(filepath: string, data: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      let localPath = path.join(this._localRoot, this._repo, filepath);
      let basePath = path.parse(localPath);

      if (!fs.existsSync(basePath.dir)) {
        let err = fs.mkdirSync(basePath.dir, { recursive: true });
        if (err != undefined) {
          reject(err);
        }
      }
      let yamlObj = yaml.parse(data); //屏蔽配置文件中的注释，防止搜索干扰
      let noComment = yaml.stringify(yamlObj);
      if (!fs.existsSync(localPath)) {
        fs.writeFileSync(localPath, noComment);
      } else {
        let local = fs.readFileSync(localPath).toString();
        let locaMd5 = Crypto.md5(local);
        let remoteMd5 = Crypto.md5(noComment);
        if (locaMd5 != remoteMd5) {
          fs.rmSync(localPath);
          fs.writeFileSync(localPath, noComment);
        }
        resolve();
      }
    });
  }
  private loadBranch(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      let branch = "master";
      if (this._repo == "win-program") {
        if (
          process.env.WIN_PROGRAM_BRANCH ||
          process.env.WINE_ASSISTANT_BRANCH
        ) {
          resolve(
            process.env.WIN_PROGRAM_BRANCH ||
              process.env.WINE_ASSISTANT_BRANCH ||
              branch
          );
          return;
        }
      } else if ((this._repo = "win-dependency")) {
        if (process.env.WIN_DEPENDENCY_BRANCH) {
          resolve(process.env.WIN_DEPENDENCY_BRANCH);
          return;
        }
      }
      let branchurl = this._repoTemplate
        .replace("#REPO#", this._repo)
        .replace("#BRANCH#", branch)
        .replace("#FILEPATH#", "release.yml"); //path.join(this._root, this._repo, branch, filepath);
      axios.get(branchurl).then(
        (resp) => {
          try {
            let obj: Release = yaml.parse(resp.data);
            resolve(obj.Branch);
          } catch (error) {
            //logger.warn(error);
            resolve(branch);
          }
        },
        (reason) => {
          //logger.warn(reason);
          resolve(branch);
        }
      );
    });
  }

  /**
   * 定义一个通用的拉取源中的配置文件接口
   * @param filepath 文件路径，需要到git源汇总查看文件的原始链接
   * @returns Promise<string> 返回配置文件原始数据
   */
  protected loadConfig(filepath: string): Promise<any> {
    if (
      import.meta.env.DEV &&
      (process.env.WIN_PROGRAM_LOCAL || process.env.WIN_DEPENDENCY_LOCAL)
    ) {
      let local;
      switch (this._repo) {
        case "win-program":
          local = process.env.WIN_PROGRAM_LOCAL;
          break;
        case "win-dependency":
          local = process.env.WIN_DEPENDENCY_LOCAL;
          break;
      }
      if (local && fs.existsSync(local)) {
        return new Promise<any>((resolve, reject) => {
          let localPath = path.join(local, filepath);
          logger.debug("load from local", localPath);
          fs.readFile(localPath, (err, buff) => {
            if (err != null) {
              logger.error(err.message);
              reject(err.message);
            } else {
              try {
                let obj = yaml.parse(buff.toString());
                resolve(obj);
              } catch (error: any) {
                logger.error(error);
                reject(error.message);
              }
            }
          });
        });
      }
    }

    let memCacheKey = `${this._repo}/${filepath}`;
    if (
      this._memoryCache[memCacheKey] !== undefined &&
      this._memoryCache[memCacheKey] != ""
    ) {
      return new Promise<any>((resolve, reject) => {
        try {
          let obj = yaml.parse(this._memoryCache[memCacheKey]);
          resolve(obj);
        } catch (error: any) {
          logger.error(error);
          //resolve(undefined);
          reject(error.message);
        }
        logger.debug("load from memory cache", memCacheKey);
      });
    }

    return new Promise<any>((resolve, reject) => {
      let branch = "master";
      if (this._repo == "win-program") {
        branch = `${process.env.WINE_ASSISTANT_BRANCH || "master"}`;
      }
      let branchurl = this._repoTemplate
        .replace("#REPO#", this._repo)
        .replace("#BRANCH#", branch)
        .replace("#FILEPATH#", "release.yml"); //path.join(this._root, this._repo, branch, filepath);

      //axios.get(branchurl).then((resp) => {
      //try {
      //let obj: Release = yaml.parse(resp.data);
      this.loadBranch().then((branch) => {
        let url = this._repoTemplate
          .replace("#REPO#", this._repo)
          .replace("#BRANCH#", branch)
          .replace("#FILEPATH#", filepath); //path.join(this._root, this._repo, branch, filepath);
        logger.debug("load config from :", url);
        axios
          .get(url)
          .then((resp) => {
            //resolve(resp.data);
            this._memoryCache[memCacheKey] = resp.data;
            try {
              let obj = yaml.parse(this._memoryCache[memCacheKey]);
              resolve(obj);
            } catch (error: any) {
              logger.error(error);
              //resolve(undefined);
              reject(error.message);
            }
          })
          .catch((err) => {
            reject(err.message);
          })
          .finally(() => {});
      });
      //} catch (error: any) {
      //logger.error(error);
      //}
      //});
    });
  }
}
