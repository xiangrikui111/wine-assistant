/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import { MessagePortMain, ipcMain } from "electron";
import { IpcProcess, IpcProtocol, IpcResult } from "../commons/define";

export class IpcManager {
  private static _instance: IpcManager;
  private _mainPort: MessagePortMain | undefined;
  private _process: Map<string, IpcProcess> = new Map<string, IpcProcess>();

  constructor() {}
  public static getInstance(): IpcManager {
    if (this._instance == undefined) {
      this._instance = new IpcManager();
    }
    return this._instance;
  }
  public regProcess(channel: string, process: IpcProcess) {
    this._process.set(channel, process);
  }
  public shutdown() {
    this._process.forEach((value, key) => {
      value.distory();
    });
  }
  /**
   * 向ui推送数据
   * @param act 推送消息actoin
   * @param data 推送数据
   */
  public static sendToView(act: string, data: any) {
    let result = {} as IpcResult;
    result.Action = act;
    result.Class = "pushMessage";
    result.Data = data;
    this._instance._mainPort?.postMessage(result);
  }
  public startup() {
    ipcMain.on("port", (event) => {
      if (this._mainPort != undefined) {
        this._mainPort.close();
      }
      this._mainPort = event.ports[0];
      if (this._mainPort != undefined) {
        this._mainPort.on("message", (event) => {
          let data: IpcProtocol = event.data as IpcProtocol;
          let result = event.data as IpcResult;
          result.Code = 0;
          //this._rendererPort?.postMessage("hello");
          let processer = this._process.get(data.Class);
          if (processer == undefined) {
            result.Code = -1;
            result.Data = "Unsupport class";
            this._mainPort?.postMessage(result);
            return;
          }
          processer
            .ipcProcess(data.Action, data.Data)
            .then((r) => {
              result.Data = r;
              this._mainPort?.postMessage(result);
            })
            .catch((reason) => {
              result.Code = -1;
              result.Data = reason;
              this._mainPort?.postMessage(result);
            });
        });
        this._mainPort.start();
      }
    });
  }
}
