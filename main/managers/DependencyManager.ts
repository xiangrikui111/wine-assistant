/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import { BaseManager } from "./Base";
import * as yaml from "yaml";
import { DependencyInfo, DependencyRun, Step } from "../commons/define";
import {
  DependencyOverrideDllStep,
  DependencyUninstallStep,
  DependencySetWindowsStep,
  DependencyCabExtractStep,
  DependencyExtractStep,
  DependencyReplaceFontStep,
  DependencyRegisterFontStep,
  DependencyInstallFontsStep,
  DependencyCopyFileStep,
  DependencyInstallStep,
} from "../internal/DependencySteps";
import { logger } from "../utils/Logger";

export class DependencyManager extends BaseManager {
  private _dependencies: DependencyInfo[] = [] as DependencyInfo[];
  public ipcProcess(action: string, _data: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      switch (action) {
        case "getDependencies":
          this.getDependencies().then(
            (ds) => {
              resolve(ds);
            },
            (err) => {
              reject(err);
            }
          );
          break;
        default:
          reject("Unsupport action");
      }
    });
  }

  protected _repo: string = "win-dependency";
  private static instance: DependencyManager;
  private constructor() {
    super();
  }
  public static getInstance() {
    if (!this.instance) {
      this.instance = new DependencyManager();
    }
    return this.instance;
  }

  /**
   * 获取Windows依赖列表
   * @param pageno 分页页码
   * @param pagesize 分页大小
   * @example Promise<DependencyInfo[]>
   * ```ts
   * DependencyManager.getInstance().getDependencies().then(result=>{
   *   // do somathing
   * },err=>{
   *   // do error
   * })
   * ```
   */
  public getDependencies(depKeys?: string[]): Promise<DependencyInfo[]> {
    const filterFunc = (item: DependencyInfo) => {
      if (depKeys === undefined) {
        return true;
      }
      /*
      if (depKeys.length == 0) {
        return false;
      }
      */
      let findIdx = depKeys.findIndex((val) => {
        return val === item.Key;
      });
      return findIdx >= 0;
    };
    return new Promise<DependencyInfo[]>((res, rej) => {
      if (this._dependencies.length == 0) {
        this.loadConfig("index.yml").then(
          (data) => {
            for (let k in data) {
              let dInfo = Object.assign(new DependencyInfo(), data[k]);
              dInfo.Key = k;
              dInfo.Name = k;
              let idx = this._dependencies.findIndex((val) => {
                return val.Key === k;
              });
              if (idx < 0) {
                this._dependencies.push(dInfo);
              }
            }
            res(this._dependencies.filter(filterFunc));
          },
          (err) => {
            rej(err);
          }
        );
      } else {
        let r = this._dependencies.filter(filterFunc);
        //res(this._dependencies.filter(filterFunc));
        res(r);
      }
    });
  }

  /**
   * 获取依赖运行信息
   * @param name 依赖名称
   * @returns Promise<DependencyRun>
   * @example
   * ```ts
   * DependencyManager.getInstance().getDependencyRun("d3dcompiler_47").then(result=>{
   *   //do somathing
   * },err=>{
   *   //do error
   * })
   * ```
   */
  public getDependencyRun(
    category: string,
    key: string
  ): Promise<DependencyRun> {
    return new Promise<DependencyRun>((res, rej) => {
      this.loadConfig(`${category}/${key}.yml`).then(
        (data) => {
          //Object.assign不能深拷贝，所以提前先吧step转成class
          let steps = [] as Step[];
          data["Steps"].forEach((s) => {
            //console.log(s, i);
            switch (s.action) {
              case "override_dll":
                steps.push(Object.assign(new DependencyOverrideDllStep(), s));
                break;
              case "uninstall":
                steps.push(Object.assign(new DependencyUninstallStep(), s));
                break;
              case "set_windows":
                steps.push(Object.assign(new DependencySetWindowsStep(), s));
                break;
              case "cab_extract":
                steps.push(Object.assign(new DependencyCabExtractStep(), s));
                break;
              case "archive_extract":
                steps.push(Object.assign(new DependencyExtractStep(), s));
                break;
              case "replace_font":
                steps.push(Object.assign(new DependencyReplaceFontStep(), s));
                break;
              case "register_font":
                steps.push(Object.assign(new DependencyRegisterFontStep(), s));
                break;
              case "install_fonts":
                steps.push(Object.assign(new DependencyInstallFontsStep(), s));
                break;
              case "copy_file":
                steps.push(Object.assign(new DependencyCopyFileStep(), s));
                break;
              case "install":
                steps.push(Object.assign(new DependencyInstallStep(), s));
                break;
            }
          });
          let dRun: DependencyRun = Object.assign(new DependencyRun(), data);
          dRun.Key = key;
          dRun.Steps = steps;

          /*
          console.log(
            `----------type=${JSON.stringify(dRun, undefined, 2)}--------------`
          );
          */
          res(dRun);
        },
        (err: any) => {
          logger.error(`load config error:${err}`);
          rej(err);
        }
      );
    });
  }
}
