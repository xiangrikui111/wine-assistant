/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import { FileType, Step, KeyStep, InstallStatus } from "../commons/define";
import * as Path from "path";
// import * as fs from "fs";
import { WineCommand } from "../utils/WineCommand";
import { DownloaderManager, FileInfo } from "../managers/DownloaderManager";
import { logger } from "../utils/Logger";
import { rimraf } from "rimraf";
import * as fs from "fs-extra";
import * as path from "path";
import { UnpackFile } from "../utils/ArchiveUtil";
import { Uninstaller } from "../utils/Uninstaller";
import { StatusManager } from "../managers/StatusManager";
import { WorkDirConfig } from "../commons/config";
import { spawn, exec } from "child_process";

export class ProgramInstallStep extends Step {
  action: string = "install";
  file_name: string = "";
  file_type: FileType = "";
  url: string = "";
  protable: boolean | undefined;
  file_checksum: boolean | undefined | string;
  arguments: string = "";
  private _uninstaller = new Uninstaller();
  private _queued = [] as FileInfo[];
  private installProtable(fileInfo: FileInfo): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      if (this.file_name == "") {
        reject("Exec file is empty");
        return;
      }
      if (fileInfo.IsArchive) {
        let unpackPath = fileInfo.DownloadedPath + ".unpack";
        let execPath = Path.join(unpackPath, this.file_name);
        UnpackFile(fileInfo.ExtName, fileInfo.DownloadedPath, unpackPath).then(
          () => {
            if (!fs.existsSync(execPath)) {
              logger.error("File not found :" + execPath);
              reject("Install error:" + fileInfo.AppKey);
              return;
            }

            if (fs.existsSync(fileInfo.ProtableInstallPath)) {
              //fs.unlinkSync(newExecPath);
              rimraf.rimrafSync(fileInfo.ProtableInstallPath);
            }

            fs.renameSync(unpackPath, fileInfo.ProtableInstallPath);
            resolve(path.join(fileInfo.ProtableInstallPath, this.file_name));
          },
          (reason) => {
            reject(reason);
          }
        );
      } else {
        let installPath = path.join(
          fileInfo.ProtableInstallPath,
          fileInfo.FileName
        );
        try {
          if (fs.existsSync(fileInfo.ProtableInstallPath)) {
            rimraf.rimrafSync(fileInfo.ProtableInstallPath);
          }
          fs.mkdirSync(fileInfo.ProtableInstallPath, { recursive: true });
          fs.renameSync(fileInfo.DownloadedPath, installPath);
          resolve(installPath);
        } catch (error) {
          reject(error);
        }
        //resolve()
      }
    });
  }
  private installSetup(fileInfo: FileInfo): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      if (fileInfo.IsArchive) {
        let unpackPath = fileInfo.DownloadedPath + ".unpack";
        let execPath = Path.join(unpackPath, this.file_name);
        UnpackFile(fileInfo.ExtName, fileInfo.DownloadedPath, unpackPath).then(
          () => {
            const wineCmd = new WineCommand(
              //fileInfo.DownloadedPath,
              execPath,
              undefined,
              this.arguments,
              undefined
            );
            wineCmd.run().then(
              () => {
                resolve();
              },
              (reason) => {
                reject(reason);
              }
            );
            //wineCmd.run().then(res, rej);
          },
          (reason) => {
            reject(reason);
          }
        );
      } else {
        const wineCmd = new WineCommand(
          //fileInfo.DownloadedPath,
          fileInfo.DownloadedPath,
          undefined,
          this.arguments,
          undefined
        );
        wineCmd.run().then(
          () => {
            resolve();
          },
          (reason) => {
            reject(reason);
          }
        );
      }
    });
  }
  process(keyStep: KeyStep): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (this.url.trim().length == 0) {
        reject("Download url is empty");
        return;
      }
      DownloaderManager.getInstance()
        .start(keyStep.Key, this.url)
        .then(
          (fileInfo: FileInfo | undefined) => {
            if (!fileInfo) {
              reject("File not found");
              return;
            }

            if (this.protable) {
              this.installProtable(fileInfo).then(
                (_newExecPath) => {
                  StatusManager.getInstance().updateInstallStatus(
                    keyStep.Key,
                    InstallStatus.Installed,
                    _newExecPath
                  );
                  StatusManager.getInstance().updateUninstallInfo(
                    keyStep.Key,
                    fileInfo.ProtableInstallPath,
                    keyStep.Name,
                    keyStep.Name,
                    true
                  );
                  resolve("");
                },
                (reason) => {
                  reject(reason);
                }
              );
            } else {
              this._uninstaller.list().then((uninstalls) => {
                this.installSetup(fileInfo).then(
                  () => {
                    this._uninstaller.list().then(
                      (afterUninstalls) => {
                        let guids = [] as string[][];
                        //let guid = "";
                        //let displayName = "";
                        Object.entries(afterUninstalls).forEach((val) => {
                          if (!uninstalls[val[0]]) {
                            guids.push(val);
                          }
                        });
                        if (guids.length === 1) {
                          StatusManager.getInstance().updateUninstallInfo(
                            keyStep.Key,
                            guids[0][0],
                            guids[0][1],
                            keyStep.Name
                          );
                        } else if (guids.length > 1) {
                          logger.warn(
                            "Multiple registry information ",
                            keyStep.Key,
                            guids
                          );
                        }
                        resolve("");
                      },
                      (_reason) => {
                        resolve("");
                      }
                    );
                  },
                  (reason) => {
                    reject(reason);
                  }
                );
              });
            }
          },
          (reason) => {
            reject(reason);
          }
        );
    });
  }
}

/**
 * 特殊字符列表：
 * APP_PATH             会被替换位app实际安装的workdir
 * CROOT_PATH           会被替换为 容器根路径，也就是dos路径C:\\
 * 使用用例： echo "CROOTPATH=$CROOT_PATH APP_PATH=$APP_PATH PWD=$PWD"
 *
 * 另外，由于采用bash -c '${script}'方式，命令中特殊字符需要编写人员自己处理，如'符号
 */
export class ProgramRunScriptStep extends Step {
  action: string = "run_script";
  script: string = "";
  process(keyStep: KeyStep): Promise<string> {
    return new Promise<string>((res, rej) => {
      const ContrainerPath = Path.join(
        WorkDirConfig.ContainerDir,
        WorkDirConfig.ContainerName,
        "drive_c"
      );

      let APPath = ContrainerPath;
      let info = StatusManager.getInstance().getInstallInfo(keyStep.Key);
      if (
        !info ||
        info.InstallStatus !== InstallStatus.Installed ||
        !info.ExecFilePath
      ) {
        rej("Program not installed");
        return;
      }
      logger.debug(
        `key:${keyStep.Key} value: ${JSON.stringify(info, undefined, 2)}`
      );
      //APPath = execPath;

      //logger.debug(this.script);
      //res("");
      //return;
      if (this.script.trim() !== "") {
        const command = `/usr/bin/bash -c "${this.script}"`;
        const env = {
          PATH: "/usr/local/bin:/usr/bin:/bin",
          APP_PATH: `'${Path.dirname(info.ExecFilePath)}'`,
          CROOT_PATH: `'${ContrainerPath}'`,
        };
        logger.debug(`Running command: ${command}`);

        exec(
          `${command}`,
          { env, cwd: ContrainerPath },
          (error, stdout, stderr) => {
            if (error) {
              logger.error(`Run script failed: ${error.message}`);
              rej(error.message);
              return;
            }
            if (stderr) {
              logger.error(`Run script failed: ${stderr}`);
              rej(stderr);
              return;
            }
            logger.debug(`Run script output: ${stdout}`);
            res("");
          }
        );
      } else {
        res("");
      }
    });
  }
}
