/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import { Executable } from "../commons/define";
import { WorkDirConfig } from "../commons/config";
import { WineCommand } from "../utils/WineCommand";
import * as Path from "path";

/**
 * 实现一个简单的step例子，后续照着写逻辑即可
 * manager会读取社区上各个配置文件，来填充setp对象的属性字段，internal会顺序调用每个step的process方法
 */
export class ProgramExec extends Executable {
  name: string = "";
  path: string = "";
  run_in_window: boolean | string = false;
  desktop: string = "";
  wineenv: any = {};
  arguments: string = "";
  exec(execPath?: string): Promise<string> {
    return new Promise<string>((res, rej) => {
      const desktopPath = WorkDirConfig.DesktopDir;
      const filePath = execPath
        ? execPath
        : this.path.trim().endsWith(".desktop")
        ? Path.isAbsolute(this.path)
          ? this.path
          : Path.join(desktopPath, this.path)
        : WineCommand.dos2unixPath(this.path);
      const wineCmd = new WineCommand(
        filePath,
        this.wineenv,
        this.arguments,
        undefined,
        true,
        this.run_in_window
      );
      wineCmd
        .run()
        .then(res, rej)
        .finally(() => {});
    });
  }
}
