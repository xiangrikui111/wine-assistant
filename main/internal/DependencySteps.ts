/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import * as fs from "fs";
import * as Path from "path";
import * as glob from "glob";
import { Reg, RegItem } from "../utils/Reg";
import { isDirectory, renameFile } from "../utils/Utils";
import { setWindows } from "../utils/SetWine";
import { Step, KeyStep, DownloadStatus } from "../commons/define";
import { WineCommand } from "../utils/WineCommand";
import { WorkDirConfig } from "../commons/config";
import { DownloaderManager, FileInfo } from "../managers/DownloaderManager";
import { logger } from "../utils/Logger";
import { UnpackFile } from "../utils/ArchiveUtil";

// key: HKLM\\Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full
// value: Version
// data: 4.0.30319
// type: REG_SZ
export class DependencyRegisterKeyStep extends Step {
  action: string = "set_register_key";
  key: string = "";
  value: string = "";
  data: string = "";
  type: string = "";
  process(): Promise<string> {
    const reg = new Reg();
    logger.debug(`-------${this.action}--------`);
    return reg.add(this.key, this.value, this.data, this.type);
  }
}

export class DependencyOverrideDllStep extends Step {
  action: string = "override_dll";
  dll?: string = "";
  type?: string = "";
  bundle?: { value: string; data: string }[];

  process(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const reg = new Reg();
      logger.debug(`-------${this.action}--------`);
      if (this.bundle) {
        const bundle = {
          "HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides": this.bundle.map(
            (item) => {
              return {
                value: item.value,
                data: item.data,
              };
            }
          ),
        };
        reg
          .importBundle(bundle)
          .then(() => {
            resolve("Register a new override for each dll successfully.");
          })
          .catch((err) => {
            reject(`Failed to register a new override for each dll: ${err}`);
          });
      } else if (
        typeof this.dll === "string" &&
        typeof this.type === "string"
      ) {
        reg
          .add(
            "HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides",
            this.dll || "",
            this.type || ""
          )
          .then(() => {
            resolve("Register a new override for each dll successfully.");
          })
          .catch((err) => {
            reject(`Failed to register a new override for each dll: ${err}`);
          });
      } else {
        reject("Invalid parameters in DependencyOverrideDllStep.");
      }
    });
  }
}

export class DependencyCopyFileStep extends Step {
  action: string = "copy_file";
  url: string = ""; // 源文件所在的目录，必须与下载解压step的目录对应
  file_name: string = ""; // 可以包含通配符如 a*.dll
  dest: string = ""; // dest 规定必须是从C:\\ 开始的路径

  process(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      if (this.dest.trim().length === 0) {
        logger.error(`Failed to copy file, dest is empty: ${this.dest}`);
        reject(`Failed to copy file, dest is empty: ${this.dest}`);
        return;
      }
      const destPath = Path.join(
        WorkDirConfig.ContainerDir,
        WorkDirConfig.ContainerName,
        "drive_c",
        this.dest
      );
      logger.debug(`-------${this.action}--------`);
      if (!fs.existsSync(destPath)) {
        fs.mkdirSync(destPath, { recursive: true });
      }

      const srcPath = Path.join(WorkDirConfig.DownloadedDir, this.url);

      let srcFiles: string[];

      if (this.file_name.includes("*")) {
        // 如果文件名包含通配符，则使用 glob 模块获取所有符合通配符的文件路径
        srcFiles = glob.sync(Path.join(srcPath, this.file_name));
      } else {
        // 否则，将 file_name 转换成一个路径
        srcFiles = [Path.join(srcPath, this.file_name)];
      }

      const promises = srcFiles.map((srcFile) => {
        const destFile = Path.join(destPath, Path.basename(srcFile));

        return new Promise((resolve, reject) => {
          // 如果目标文件已经存在，则将目标文件重命名为 .bak 后缀
          if (fs.existsSync(destFile)) {
            fs.renameSync(destFile, destFile + ".bak");
          }

          fs.copyFile(srcFile, destFile, (err) => {
            if (err) {
              logger.error(`Failed to copy file ${srcFile}: ${err}`);
              reject(`Failed to copy file ${srcFile}: ${err}`);
            } else {
              resolve(`File ${srcFile} is installed.`);
            }
          });
        }) as never;
      });

      Promise.all(promises)
        .then((_results) => {
          resolve("copy files completed.");
        })
        .catch((err) => {
          reject(`Failed to copy files: ${err}`);
        });
    });
  }
}

// 将字体文件拷贝到容器的windows下的字体目录中
export class DependencyInstallFontsStep extends Step {
  action: string = "install_fonts";
  url: string = "";
  fonts: string[] = [];
  process(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const fontPath = Path.join(
        WorkDirConfig.ContainerDir,
        WorkDirConfig.ContainerName,
        "drive_c",
        "windows",
        "Fonts"
      );
      logger.debug(`-------${this.action}--------`);
      if (!fs.existsSync(fontPath)) {
        fs.mkdirSync(fontPath, { recursive: true });
      }

      const promises = [];
      const srcFilePath = Path.join(WorkDirConfig.DownloadedDir, this.url);
      logger.debug(
        `install fonts: ${srcFilePath} fonts:${JSON.stringify(
          this.fonts,
          undefined,
          2
        )}`
      );
      for (const font of this.fonts) {
        const destFilePath = Path.join(fontPath, font);

        promises.push(
          new Promise((resolve, reject) => {
            // 如果目标文件已经存在，则先删除目标文件
            if (fs.existsSync(destFilePath)) {
              fs.unlinkSync(destFilePath);
            }
            let srcPath = srcFilePath;
            if (isDirectory(srcPath)) {
              srcPath = Path.join(srcPath, font);
            }
            logger.debug(`copy fonts: ${srcPath} --> ${destFilePath}`);
            fs.copyFile(srcPath, destFilePath, (err) => {
              if (err) {
                logger.error(`Failed to install Font ${font}: ${err}`);
                reject(`Failed to install Font ${font}: ${err}`);
              } else {
                resolve(`Font ${font} is installed.`);
              }
            });
          }) as never
        );
      }

      Promise.all(promises)
        .then((_results) => {
          resolve("install fonts completed.");
        })
        .catch((err) => {
          reject(`Failed to install fonts: ${err}`);
        });
    });
  }
}

export class DependencyRegisterFontStep extends Step {
  action: string = "register_font";
  name: string = "";
  file: string = "";
  process(): Promise<string> {
    const reg = new Reg();
    logger.debug(`-------${this.action}--------`);
    return reg.add(
      "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Fonts",
      this.name,
      this.file
    );
  }
}

export class DependencyReplaceFontStep extends Step {
  action: string = "replace_font";
  font: string = "";
  replace: string[] = [];

  process(): Promise<string> {
    const reg = new Reg();
    logger.debug(`-------${this.action}--------`);
    const regs = this.replace.map((r: string) => {
      return new RegItem(
        "HKEY_CURRENT_USER\\Software\\Wine\\Fonts\\Replacements",
        r,
        "",
        this.font
      );
    });

    return reg.batchAdd(regs);
  }
}

// 下载/解压放在一个step，安装、注册放在后续step
// cab_extract 和 archive_extract 使用一个Step，除action不同外均相同
class ExtractStep extends Step {
  action: string = "";
  url: string = "";
  file_name: string = "";
  file_checksum: string = "";
  file_size: number = 0;
  dest: string = "";
  rename: string = "";
  process(keyStep: KeyStep): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const downloader: DownloaderManager = DownloaderManager.getInstance();

      downloader
        .start(keyStep.Key, this.url)
        .then((fileInfo: FileInfo | undefined) => {
          //logger.info(fileInfo);
          //let fileKey = Crypto.md5(this.url);
          //let fileInfo = downloader.getFileInfo(fileKey);
          if (
            fileInfo !== undefined &&
            fileInfo.DownloadStatus == DownloadStatus.Downloaded
          ) {
            //let filePath = fileInfo.DownloadedPath;
            /*
             */
            let ext: string = fileInfo.ExtName;
            if (this.action === "cab_extract") {
              ext = "cab";
            }
            if (this.dest.trim().length === 0) {
              this.dest = "temp";
            }
            let dstPath = Path.join(WorkDirConfig.DownloadedDir, this.dest);
            UnpackFile(ext, fileInfo.DownloadedPath, dstPath).then(
              () => {
                if (this.rename.trim().length != 0) {
                  renameFile(
                    dstPath,
                    Path.join(WorkDirConfig.DownloadedDir, this.rename)
                  );
                }
                resolve("Extraction completed");
              },
              (reason) => {
                reject(reason);
              }
            );
            /*
            const detector = new FileTypeDetector(filePath, ext);
            detector
              .detector(
                Path.join(WorkDirConfig.DownloadedDir, this.dest),
                () => {
                  filePath = Path.join(WorkDirConfig.DownloadedDir, this.dest);
                  logger.debug("Extraction completed");
                }
              )
              .then(
                (ret) => {
                  logger.debug(`Extraction result: ${ret}`);
                  if (this.rename.trim().length != 0) {
                    renameFile(
                      filePath,
                      Path.join(WorkDirConfig.DownloadedDir, this.rename)
                    );
                  }
                  resolve("Extraction completed");
                },
                (error) => {
                  reject(error.message);
                }
              );
              */
          } else {
            logger.error("on ------");
            reject("Undownload");
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}

// 下载/解压放在一个step，安装、注册放在后续step
// cab_extract 和 archive_extract 使用一个Step，除action不同外均相同
export class DependencyExtractStep extends ExtractStep {
  action: string = "archive_extract";
}

// 下载/解压放在一个step，安装、注册放在后续step
// cab_extract 和 archive_extract 使用一个Step，除action不同外均相同
export class DependencyCabExtractStep extends ExtractStep {
  action: string = "cab_extract";
}

export class DependencySetWindowsStep extends Step {
  action: string = "set_windows";
  version: string = "";
  process(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      logger.debug(`-------${this.action}--------`);
      if (this.version === "") {
        reject("version error!");
      } else {
        setWindows(this.version).then(resolve, reject);
      }
    });
  }
}

export class DependencyUninstallStep extends Step {
  action: string = "uninstall";
  file_name: string = "";
  process(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      logger.debug(`-------${this.action}--------`);
      if (this.file_name === "") {
        reject("file_name error!");
      } else {
        logger.debug(`uninstall ${this.file_name}`);
        resolve("ok");
      }
    });
  }
}

export class DependencyInstallStep extends Step {
  action: string = "install";
  file_name: string = "";
  url: string = "";
  file_checksum: boolean | undefined | string;
  arguments: string = "";
  environment: any = {};
  process(keyStep: KeyStep): Promise<string> {
    //throw new Error("Method not implemented.");
    return new Promise<string>((res, rej) => {
      /*
      if (this.file_name.trim() === "") {
        logger.error(`file_name 字段不能为空：${this.file_name.trim()}`);
        rej("file_name 字段不能为空！");
        return;
      }
      */
      logger.debug(`-------${this.action}--------`);
      if (this.url.trim().length !== 0) {
        const downloader: DownloaderManager = DownloaderManager.getInstance();

        downloader
          .start(keyStep.Key, this.url)
          .then((fileInfo: FileInfo | undefined) => {
            if (fileInfo == undefined) {
              logger.error("Download file error !", keyStep.Key);
              rej("Download file error: " + keyStep.Key);
              return;
            }
            if (fileInfo.IsArchive) {
              let unpackPath = fileInfo.DownloadedPath + ".unpack";
              UnpackFile(
                fileInfo.ExtName,
                fileInfo.DownloadedPath,
                unpackPath
              ).then(
                () => {
                  if (this.file_name == "") {
                    rej("Exec file name empty");
                    return;
                  }
                  let filePath = Path.join(unpackPath, this.file_name);
                  const wineCmd = new WineCommand(
                    filePath,
                    this.environment,
                    this.arguments,
                    undefined
                  );

                  wineCmd.run().then(res, rej);
                },
                (reason) => {
                  rej(reason);
                }
              );
            } else {
              const wineCmd = new WineCommand(
                fileInfo.DownloadedPath,
                this.environment,
                this.arguments,
                undefined
              );

              wineCmd.run().then(res, rej);
            }
            /*
            let filePath = fileInfo.DownloadedPath;
            const detector = new FileTypeDetector(filePath);
            detector
              .detector(filePath + ".unpack", () => {
                filePath = Path.join(
                  filePath + ".unpack",
                  this.file_name || ""
                );
                logger.debug("Extraction completed");
              })
              .then(
                (ret) => {
                  logger.debug(`Extraction result: ${ret}`);
                  const wineCmd = new WineCommand(
                    filePath,
                    this.environment,
                    this.arguments,
                    undefined
                  );

                  wineCmd.run().then(res, rej);
                },
                (error) => {
                  rej(error.message);
                }
              );
              */
          })
          .catch((error) => {
            rej(error);
          });
      }
    });
  }
}
