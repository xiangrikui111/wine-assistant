/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import { ParamInternal, ProgramCategory, ProgramRun } from "../commons/define";
import { Executable, InstallStatus } from "../commons/define";
import { ProgramManager } from "../managers/ProgramManager";
import { WineCommand } from "../utils/WineCommand";
import { StatusManager } from "../managers/StatusManager";
import { BaseInternal } from "./Base";
import { generateDesktop, removeDesktop } from "../utils/Utils";
import { logger } from "../utils/Logger";
import { Uninstaller } from "../utils/Uninstaller";
import { rimraf } from "rimraf";
import { WorkDirConfig } from "../commons/config";
import * as fs from "fs";
import {
  ProgramInstallStep,
} from "../internal/ProgramSetps";

//type ActionHandle = (run: ProgramRun) => Promise<string>;

export class ProgramInternal extends BaseInternal {
  private _statusManager = StatusManager.getInstance();
  private _singleInstallLock: string = "";
  private afterInstall(run: ProgramRun): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      if (this._statusManager.isInstalled(run.Key)) {
        //检查应用是否有桌面文件，如果没有创建一个，例如YNOTE应用
        //if (!ExistDesktop(run.Name)) {
        let info = this._statusManager.getInstallInfo(run.Key);
        //通过文件监听，获取到可执行程序绝对路径
        const execPath = info?.ExecFilePath;
        if (execPath) {
          const wineCmd = new WineCommand(
            execPath,
            undefined,
            run.Executable?.arguments,
            undefined,
            true,
            run.Executable?.run_in_window
          );
          const [errorMsg, cmdstr] = wineCmd.buildCommand();
          if (errorMsg !== "") {
            reject(errorMsg);
            return;
          } else {
            generateDesktop(
              info?.UninstallInfo?.DesktopName || run.Name,
              run.Description,
              cmdstr,
              execPath
            ).then(
              () => {
                resolve();
              },
              (reason) => {
                logger.error("Generate desktop error", reason);
                resolve();
              }
            );
          }
        } else {
          reject("Uninstall");
        }
        //}
      } else {
        this._statusManager.updateInstallStatus(
          run.Key,
          InstallStatus.Uninstall
        );
        reject("Uninstall");
      }
    });
  }
  /*private updateProgramRun(run: ProgramRun): Promise<any> {

  } */
  private install(run: ProgramRun): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      //更新安装中状态
      this._statusManager.updateInstallStatus(
        run.Key,
        InstallStatus.Installing
      );
      //存在Executable，监听name
      if (run.Executable) {
        this._statusManager.addWatch(run.Key, run.Executable.name);
      } else {
        reject("Application information error");
        return;
      } //不存在是否应该reject?

      //调用安装接口，顺序执行配置文件中的step
      this.runSteps(run)
        .then((_res) => {
          //等待结果返回，判断已安装
          this.afterInstall(run).then(resolve, reject);
        })
        .catch((err) => {
          logger.error(err);
          this.afterInstall(run).then(() => {
            logger.warn(
              "The application has been installed, but there are exceptions in other steps ",
              run.Key
            );
            resolve("");
          }, reject);
        });
    });
  }
  /**
   * ipcMain将调用此函数处理前段的请求, 实现逻辑同Manager
   * @param action
   * @param param
   */
  public ipcProcess(action: string, param: ParamInternal): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      switch (action) {
        case "install":
          ProgramManager.getInstance()
            .getProgramRun(param.Category as ProgramCategory, param.Key)
            .then(
              (run: ProgramRun) => {
               // run.Steps[0].action=='install'
                for (let i = 0; i < run.Steps.length; i++) {
                  if (run.Steps[i].action == "install") {
                    if(param.RealUrl){
                      logger.debug('original url=', (run.Steps[i] as ProgramInstallStep).url);
                      logger.debug('new url=',param.RealUrl);
                      (run.Steps[i] as ProgramInstallStep).url = param.RealUrl;
                    }
                    break;
                  }
                }
                if (!this._statusManager.isInstalled(run.Key)) {
                  if (this._singleInstallLock != "") {
                    //为了获取应用的guid，一次只能安装一个应用
                    reject(
                      "There are currently installation tasks in progress, please wait..."
                    );
                    return;
                  }
                  this._singleInstallLock = run.Key;
                  this.install(run).then(
                    () => {
                      this._singleInstallLock = "";
                      resolve({ Key: run.Key, Message: "" });
                    },
                    (reason) => {
                      this._singleInstallLock = "";
                      reject(reason);
                    }
                  );
                } else {
                  this.exec(run).then(
                    (_message) => {
                      resolve({ Key: run.Key, Message: "" });
                    },
                    (reason) => {
                      reject(reason);
                    }
                  );
                }
              },
              (reason) => {
                this._singleInstallLock = "";
                logger.error("on internal ", reason);
                reject(reason);
              }
            );
          break;
        case "uninstall":
          ProgramManager.getInstance()
            .getProgramRun(param.Category as ProgramCategory, param.Key)
            .then(
              (run: ProgramRun) => {
                let info = this._statusManager.getInstallInfo(run.Key);
                if (info && info.InstallStatus == InstallStatus.Installed) {
                  if (info.IsProtable) {
                    if (
                      info.UninstallInfo?.UninstallKey != (undefined || "") &&
                      info.UninstallInfo.UninstallKey.startsWith(
                        WorkDirConfig.ProtableDir
                      )
                    ) {
                      //控制严格一点防止篡改配置文件，路径必须是ProtableDir开头才会删除
                      rimraf.rimrafSync(info.UninstallInfo.UninstallKey);
                      removeDesktop(info.UninstallInfo.DesktopName);
                    } else if (
                      info.ExecFilePath &&
                      fs.existsSync(info.ExecFilePath)
                    ) {
                      //兼容不支持UninstallInfo 的老版本
                      fs.unlinkSync(info.ExecFilePath);
                      removeDesktop(run.Name);
                    }
                    resolve("");
                  } else {
                    let uninstaller = new Uninstaller();
                    if (info.UninstallInfo?.UninstallKey == (undefined || "")) {
                      //兼容不支持UninstallInfo 的老版本
                      //let control = new Control();
                      //control.appwiz();
                      uninstaller.panel();
                      this._statusManager.addUninstall(run.Key, run.Name);
                      resolve("");
                    } else {
                      uninstaller
                        .remove(info.UninstallInfo.UninstallKey)
                        .then(
                          (res) => {
                            this._statusManager.addUninstall(run.Key, run.Name);
                            resolve("");
                          },
                          (reason) => {
                            logger.error(reason);
                            reject(reason);
                          }
                        )
                        .catch((err) => {
                          logger.error(err);
                        });
                    }
                  }
                }
              },
              (reason) => {
                logger.error(reason);
                reject(reason);
              }
            )
            .catch((err) => {
              logger.error(err);
              reject(err);
            });
          break;
        default:
          reject("Unsupported action");
      }
    });
  }

  private exec(appInfo: ProgramRun): Promise<string> {
    return new Promise<string>((res, rej) => {
      var exec: Executable | undefined = appInfo?.Executable;
      let info = this._statusManager.getInstallInfo(appInfo.Key);
      if (info?.ExecFilePath && exec) {
        exec.exec(info.ExecFilePath).then(res, rej);
      } else {
        rej("Executable undefined!");
      }
    });
  }

  public distory(): void {}
}
