/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import os from "os";
import path from "path";
import { execSync } from "child_process";

class _WorkDirConfig {
  HomeDir: string = os.homedir();
  WorkDir: string = path.join(this.HomeDir, ".wine-assistant");
  ContainerDir: string = path.join(this.HomeDir, ".okylin-wine");
  ContainerConfig: string = path.join(this.WorkDir, "container.json");
  ContainerName: string = "default";
  Wine: string = "/usr/bin/okylin-wine";

  ProtableDir: string = path.join(
    this.ContainerDir,
    this.ContainerName,
    "drive_c",
    "Program Protable"
  );
  TrashDir: string = path.join(this.WorkDir, "trash");
  DownloadingDir: string = path.join(this.WorkDir, "downloading");
  DownloadedDir: string = path.join(this.WorkDir, "downloaded");
  //DownloadConfig: string = path.join(this.DownloadDir, "config.json");
  ConfigDir: string = path.join(this.WorkDir, "config");

  InstallStatusConfig: string = path.join(
    this.ConfigDir,
    "install_status.json"
  );
  DownloadStatusConfig: string = path.join(
    this.ConfigDir,
    "download_status.json"
  );
  ApplicationsDir: string = path.join(
    this.HomeDir,
    ".local/share/applications"
  );
  IconsDir: string = path.join(this.WorkDir, "icons");
  DesktopDir: string = "";
  constructor() {
    let userDirsPath = path.join(this.HomeDir, ".config", "user-dirs.dirs");
    let r = execSync(". " + userDirsPath + "&& echo $XDG_DESKTOP_DIR");
    this.DesktopDir = r.toString().replaceAll("\n", "");
  }
}

export const WorkDirConfig = new _WorkDirConfig();
