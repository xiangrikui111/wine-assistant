/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
export interface WinVersions {
  [key: string]: WinVersion;
}

export interface WinVersion {
  ProductName: string;
}

export const winVersions: WinVersions = {
  win10: {
    ProductName: "Microsoft Windows 10",
  },
  win81: {
    ProductName: "Microsoft Windows 8.1",
  },
  win8: {
    ProductName: "Microsoft Windows 8",
  },
  win7: {
    ProductName: "Microsoft Windows 7",
  },
  win2008r2: {
    ProductName: "Microsoft Windows 2008 R2",
  },
  win2008: {
    ProductName: "Microsoft Windows 2008",
  },
  win2003: {
    ProductName: "Microsoft Windows 2003",
  },
  winxp: {
    ProductName: "Microsoft Windows XP",
  },
  winxp64: {
    ProductName: "Microsoft Windows XP",
  },
  win98: {
    ProductName: "Microsoft Windows 98",
  },
  win95: {
    ProductName: "Microsoft Windows 95",
  },
};
