/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
/**
 * {title:'Name', dataIndex:"name"},
  {title:'Category', dataIndex:"category"},
  {title:'Description', dataIndex:"descr",ellipsis: true,tooltip: {position: 'left'}, width:150 },
  {title:'Grade', dataIndex:"grade"},
  {title:'Arch', dataIndex:"arch"},
  {title:'LinuxArch', dataIndex:"linuxArch"},
  {title:'Optional', slotName:'optional'}
 */
export default {
  errors: {
    depend: {
      installFail: "Installation failed",
      notExist: "Dependency not exist",
      parseFail: "Parse depends failed",
    },
    program: {
      launchFail: "Launch fail",
    },
  },
  menu: {
    programs: "Programs",
    dependencies: "Dependencies",
    components: "Components",
  },
  programs: {
    header: "Program Manager",
    body: {
      table: {
        title: {
          name: "Name",
          category: "category",
          description: "Description",
          grade: "Grade",
          arch: "Arch",
          linuxArch: "LinuxArch",
          optional: "Optional",
        },
        row: {
          installBtn: "Install",
          detailBtn: "Detail",
          downloadBtn: "Download",
          installingBtn: "Installing...",
          execBtn: "Launch",
          uninstallBtn: "Uninstall",
        },
        filter: {
          confirmBtn: "Confirm",
          resetBtn: "Reset",
          placeholder: "Please input name",
        },
      },
      modal: {
        detailDialogOK: "Download & Install",
        detailDialogCancel: "Cancel",
        detailDialogTitle: "Installation Prompt",
        detailDialogContent:
          "About to visit a third-party software store page, please download the software from the store page. Once the download is complete, Wine Assistant will automatically proceed with the installation process. <br><p style='font-style:italic'><b>Reminder:</b> When downloading software from a third-party download page, please download the application through the \"regular download\" option.",
        programTitle: "Installing program...",
        analyzeDependTitle: "Analyzing dependencies...",
        dependTitle: "Installing dependencies...",
        installed: "Installed",
        installFail: "Install fail: ",
        uninstall: "Uninstall",
        initing: "Wine runtime environment is initializing...",
        deleted:
          "The Wine runtime environment has been manually deleted. Please restart the Wine assistant to attempt fixing this issue.",
        failed:
          "The initialization of the Wine runtime environment failed. You can seek help from the OpenKylin community",
      },
    },
  },
  dependencies: {
    header: "Dependency Manager",
    body: {
      table: {
        title: {
          name: "Name",
          description: "Description",
          category: "category",
          optional: "Optional",
        },
        row: {
          installBtn: "Install",
        },
      },
    },
  },
  components: {
    header: "Component Manager",
  },
};
