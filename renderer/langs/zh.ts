/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
export default {
  errors: {
    depend: {
      installFail: "安装失败",
      notExist: "依赖不存在",
      parseFail: "解析依赖失败",
    },
    program: {
      launchFail: "启动失败",
    },
  },
  menu: {
    programs: "应用管理",
    dependencies: "依赖管理",
    components: "组件管理",
  },
  programs: {
    header: "应用管理",
    body: {
      table: {
        title: {
          name: "应用名称",
          category: "应用分类",
          description: "简介",
          grade: "适配等级",
          arch: "运行架构",
          linuxArch: "Linux适配架构",
          optional: "操作",
        },
        row: {
          installBtn: "安装",
          detailBtn: "详情",
          downloadBtn: "下载",
          installingBtn: "安装中...",
          execBtn: "启动",
          uninstallBtn: "卸载",
        },
        filter: {
          confirmBtn: "确定",
          resetBtn: "重置",
          placeholder: "请输入应用名称",
        },
      },
      modal: {
        detailDialogOK: "下载并安装",
        detailDialogCancel: "取消",
        detailDialogTitle: "安装提示",
        detailDialogContent:
          "即将前往第三方软件商店页面，请在商店页面中下载软件，当下载完成后，wine助手会自动进行后续安装过程。<br><p style='font-style:italic'><b>温馨提示：</b>在第三方下载页下载软件时，请通过\"普通下载\"的方式下载。</p>",
        programTitle: "正在安装以下应用...",
        analyzeDependTitle: "正在分析依赖关系...",
        installingBtn: "安装中...",
        dependTitle: "正在安装以下依赖...",
        installFail: "安装失败: ",
        installed: "已安装",
        uninstall: "未安装",
        initing: "Wine运行环境正在初始化中...",
        delete: "Wine运行环境被手动删除，请重启Wine助手尝试修复此问题...",
        failed: "Wine运行环境初始化失败，可以将此问题提交的OpenKylin社区...",
      },
    },
  },
  dependencies: {
    header: "依赖管理",
    body: {
      table: {
        title: {
          name: "依赖名称",
          description: "简介",
          category: "分类",
          optional: "操作",
        },
        row: {
          installBtn: "安装",
        },
      },
    },
  },
  components: {
    header: "组件管理",
  },
};
