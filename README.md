# wine-assistant

Openkylin wine 助手

## 1. 开发环境介绍

IDE 建议使用 VSCode，目建议安装插件如下：

prettier：代码格式化

volar：Vue 语法支持

eslint：语法检查

Cloudfoundry Manifest YML Support：yaml 语法高亮

> 关于代码格式化的配置详见.vscode/settings.json

## 2. 整体架构简介

本项目基于 Electron + vue3 + vite + acrodesign + electron-builder 架构开发，开发语言统一使用 Typescript

### 2.1 yarn 镜像设置

nodejs 下载依赖包时候，可以设置国内镜像来加速下载，配置方法如下：

```
yarn config set registry "https://registry.npm.taobao.org"
yarn config set electron_builder_binaries_mirror "https://npm.taobao.org/mirrors/electron-builder-binaries/"
yarn config set electron_mirror "http://npm.taobao.org/mirrors/electron/"
```

或者直接修改~/.yarnrc, 如:

```
registry "https://registry.npm.taobao.org"
electron_builder_binaries_mirror "https://npm.taobao.org/mirrors/electron-builder-binaries/"
electron_mirror "http://npm.taobao.org/mirrors/electron/"
```

> ps:其它包管理类似，yarn 只是笔者常用的软件包管理器

### 2.2 构建运行命令

```
yarn install #安装依赖
yarn dev #开发模式，可以实时构建
yarn build  #编译
yarn package #打包

#如要新增或修改命令，修改package.json中的scripts字段即可
```

## 3. 项目文件和目录介绍

### 3.1 主要配置文件介绍:

```
package.json #nodejs主配置文件
tsconfig.json #typescript编译器配置文件
vite.config.ts  #vite编译框架配置文件
```

### 3.2 主要源码目录介绍

```
renderer:UI界面源码目录
main:Electron主代码目录
preload:Electron预加载源码目录
```

## 4. 代码架构

### 4.1 UI 部分(renderer 目录)

```
renderer/
├── App.vue #页面主框架文件
├── assets #静态资源
│   └── vue.svg
├── components #Vue组件目录，主要定义各个子功能页面
│   ├── Components.vue
│   ├── Dependencies.vue
│   └── Programs.vue
├── i18n.ts #多语言支持
├── langs #多语言字典文件
│   ├── en.ts
│   └── zh.ts
├── main.ts #Vue入口主程序
├── routes.ts #Vue-router配置
├── style.css #主样式单
└── vite-env.d.ts
```

### 4.2 Electron 部分(main 目录)

```
main/
├── commons #通常用来定义一些公共类型，或常量
│   ├── catalogs.ts
│   ├── config.ts
│   └── define.ts
├── internal #调用本地程序逻辑源码目录， 如执行系统命令等
│   ├── Base.ts
│   ├── DependencyInternal.ts
│   ├── DependencySteps.ts
│   ├── ProgramExec.ts
│   ├── ProgramInternal.ts
│   └── ProgramSetps.ts
├── main.ts #Electron主程序入口
├── managers #数据管理相关源码目录，如从社区应用列表仓库拉去应用信息，查询应用信息等。。。
│   ├── Base.ts
│   ├── DependencyManager.ts
│   ├── DownloaderManager.ts
│   ├── FileStatusManager.ts
│   ├── IpcManager.ts
│   ├── ProgramManager.ts
│   └── RelationManager.ts
└── utils #通常定义一些通用的工具
    ├── Downloader.ts
    ├── FileTypeDetector.ts
    ├── ProcessManager.ts
    ├── Reg.ts
    ├── setWine.ts
    ├── Utils.ts
    ├── Wineboot.ts
    └── WineCommand.ts
```

### 4.3 Preload 部分

```
preload
└── preload.ts #目前只定义一个文件就够了
```

## 5. 构建生成目录

以下目录都是构建时生成的目录，请不要提交的 git 仓库，如以后还有其它类似目录请添加到.gitignore 文件中

```
dist:构建输出目录
node_modules:依赖安装目录
release:打包输出目录
```
## 6. 调试环境变量
1. WIN_PROGRAM_LOCAL和WIN_DEPENDENCY_LOCAL 可以使wine助手读取本地应用配置仓库和依赖配置仓库，用于调试， 如下示例配置：
```
export WIN_PROGRAM_LOCAL='/home/keming/work/code2/openKylin/win-program'
export WIN_DEPENDENCY_LOCAL='/home/keming/work/code2/openKylin/win-dependency'
```
2.WINE_ASSISTANT_DEBUG变量可以打开调试模式，目前打开调试模式的作用是，wine助手可以显示Publish为False的应用：设置如下：
```
export WINE_ASSISTANT_DEBUG=true #打开调试
export WINE_ASSISTANT_DEBUG=false #关闭调试
```
3.可以通过 WIN_PROGRAM_BRANCH环境变量来指定调试的win-program分支，例如
export WIN_PROGRAM_BRANCH=0823_dev