/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
//import { ProgramCategory } from "../main/commons/define";

//import { ActionEnum } from "../main/commons/define";

type ipcCallback = (event: Electron.IpcRendererEvent, ...args: any[]) => void;

interface Window {
  assistantApi: {
    getPrograms: (callback?: viewCallback) => void;
    getDependencies: (param: any, callback?: viewCallback) => void;
    installDependencies: (
      keys: {}[],
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
    recursiveProgramDeps: (
      category: string,
      key: string,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
    install: (
      category: ProgramCategory,
      key: string,
      RealUrl? :string,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
    openDetail: (
      param: any,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;

    uninstall: (
      category: ProgramCategory,
      key: string,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
    opDownload: (
      appKey: string,
      fileKey: string,
      op: ActionEnum,
      callback?,
      errCallback?
    ) => void;
    exec: (
      category: ProgramCategory,
      key: string,
      callback?: viewCallback
    ) => void;

    getFileStatus: (keys?: string[], callback?: viewCallback) => void;
    getProgress: (keys?: string[], callback?: viewCallback) => void;
    sysInfo: (callback?: viewCallback) => void;
    regHandler: (action: string, name: string, handler: viewCallback) => void;
  };
}

interface DownloadInfo {
  Key: string;
  File: string;
  Status: DownloadStatus;
  Progress: number;
}
/**
 * public Key: string = ""; //应用程序名称
  public File: string = ""; //对应的下载文件名
  public Status: DownloadStatus = DownloadStatus.Undownload;
    public Progress: number = 0.0; //下载百分比 0 - 100

 */
