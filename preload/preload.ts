/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import { contextBridge, ipcRenderer } from "electron";
import { Notification } from "@arco-design/web-vue";
import {
  ActionEnum,
  DependencyCategory,
  IpcProtocol,
  IpcResult,
  OpDownloadParam,
  ParamInternal,
  ProgramCategory,
} from "../main/commons/define";

type viewCallback = (data: any) => void;

let portRenderer: MessagePort | undefined = undefined;
const callbacks: Map<string, viewCallback> = new Map<string, viewCallback>();
const errCallbacks: Map<string, viewCallback> = new Map<string, viewCallback>();
const pushHandler: Map<string, Map<string, viewCallback>> = new Map<
  string,
  Map<string, viewCallback>
>();

//contextBridge.exposeInMainWorld("acquireAssistantApi", acquireAssistantApi);
let myPostMessage = (
  cls: string,
  action: string,
  param: any,
  callback?: viewCallback,
  errCallback?: viewCallback
) => {
  acquire();
  let data = {} as IpcProtocol;
  data.Class = cls;
  data.Action = action;
  data.Data = param;
  if (callback != undefined) {
    callbacks.set(data.Class + data.Action, callback);
  }
  if (errCallback != undefined) {
    errCallbacks.set(data.Class + data.Action, errCallback);
  }
  portRenderer?.postMessage(data);
};
let acquire = () => {
  if (portRenderer == undefined) {
    const { port1, port2 } = new MessageChannel();
    ipcRenderer.postMessage("port", null, [port1]);
    portRenderer = port2;
    portRenderer.onmessage = (ev: MessageEvent<any>) => {
      let result = ev.data as IpcResult;
      if (result == null) {
        return;
      }
      if (result.Code < 0) {
        let ecb = errCallbacks.get(result.Class + result.Action);
        if (ecb) {
          ecb(result.Data);
        } else {
          Notification.error(result.Data);
        }
      } else {
        if (result.Class == "pushMessage") {
          pushHandler.get(result.Action)?.forEach((cb) => {
            cb(result.Data);
          });
        } else {
          let cb = callbacks.get(result.Class + result.Action);
          /*
          if (result.Action != "getProgress") {
            console.log(result);
          }
          */
          //console.log(result);
          if (cb != undefined) {
            cb(result.Data);
          }
        }
      }
    };
  }
};

contextBridge.exposeInMainWorld("assistantApi", {
  getPrograms: (callback?: viewCallback) => {
    myPostMessage("program", "getPrograms", undefined, callback);
  },
  getDependencies: (param: any, callback?: viewCallback) => {
    myPostMessage("dependency", "getDependencies", param, callback);
  },
  openDetail: (param: any, callback?: viewCallback) => {
    myPostMessage("program", "openDetail", param, callback);
  },
  recursiveProgramDeps: (
    category: string,
    key: string,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {} as ParamInternal;
    param.Key = key;
    param.Category = category as DependencyCategory;
    myPostMessage("program", "getDependencies", param, callback, errCallback);
  },

  install: (
    category: ProgramCategory,
    key: string,
    RealUrl?: string,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {} as ParamInternal;
    param.Key = key;
    param.Category = category;
    param.RealUrl = RealUrl;
    myPostMessage("programInternal", "install", param, callback, errCallback);
  },
  uninstall: (
    category: ProgramCategory,
    key: string,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {} as ParamInternal;
    param.Key = key;
    param.Category = category;
    myPostMessage("programInternal", "uninstall", param, callback, errCallback);
  },
  exec: (
    category: ProgramCategory,
    key: string,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {} as ParamInternal;
    param.Key = key;
    param.Category = category;
    myPostMessage("programInternal", "exec", param, callback, errCallback);
  },
  opDownload: (
    appKey: string,
    fileKey: string,
    op: ActionEnum,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {} as OpDownloadParam;
    param.AppKey = appKey;
    param.FileKey = fileKey;
    param.Action = op;
    myPostMessage(
      "downloaderManager",
      "opDownload",
      param,
      callback,
      errCallback
    );
  },
  getFileStatus: (keys?: string[], callback?: viewCallback) => {
    let param = keys;
    myPostMessage("statManager", "getInstallStatus", param, callback);
  },
  getProgress: (keys?: string[], callback?: viewCallback) => {
    let param = keys;
    myPostMessage("downloaderManager", "getProgress", param, callback);
  },
  regHandler: (action: string, name: string, handler: viewCallback) => {
    if (!pushHandler.has(action)) {
      pushHandler.set(action, new Map<string, viewCallback>());
    }
    pushHandler.get(action)?.set(name, handler);
  },
  installDependencies: (
    keys: {}[],
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    myPostMessage("dependencyInternal", "install", keys, callback, errCallback);
  },
  sysInfo: (callback: viewCallback) => {
    myPostMessage("sysInfo", "getSysInfo", "", callback);
  },
});
